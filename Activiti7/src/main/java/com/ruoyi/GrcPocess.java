package com.ruoyi;

import org.activiti.bpmn.model.*;
import org.activiti.bpmn.model.Process;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class GrcPocess {
    private BpmnModel bpmnModel=null;
    private Process process=null;
    //线
    private LinkedList<SequenceFlow> lineList = new LinkedList<SequenceFlow>();
    //用户
    private LinkedList<UserTask> userList = new LinkedList<UserTask>();
    //网关
    private LinkedList<Gateway> gatewayList = new LinkedList<Gateway>();
    //开始
    private LinkedList<StartEvent> startList = new LinkedList<StartEvent>();
    //结束
    private LinkedList<EndEvent> endList = new LinkedList<EndEvent>();
    //所有
    private HashMap<String, FlowElement> idElementMap = new HashMap<>();

    public GrcPocess() {
    }

    public GrcPocess(BpmnModel bpmnModel, Process process, LinkedList<SequenceFlow> lineList, LinkedList<UserTask> userList, LinkedList<Gateway> gatewayList, LinkedList<StartEvent> startList, LinkedList<EndEvent> endList,HashMap<String, FlowElement> idElementMap) {
        this.bpmnModel = bpmnModel;
        this.process = process;
        this.lineList = lineList;
        this.userList = userList;
        this.gatewayList = gatewayList;
        this.startList = startList;
        this.endList = endList;
        this.idElementMap = idElementMap;
    }
    public UserTask getUserTaskByActivityId(String activityId){
        for (UserTask userTask : userList) {
            if( userTask.getId().equals(activityId)){
                return userTask;
            }
        }
        return  null;
    }



    public List<GrcUserTask> getPreUserTask(String activityId){
        LinkedList<GrcUserTask> userTasks = new LinkedList<>();
        UserTask userTask = getUserTaskByActivityId(activityId);
        if(userTask!=null){
            for (UserTask task : userList) {
                if( task.getId().equals(activityId)){
                    return userTasks;
                }else{
                    userTasks.add(new GrcUserTask(task));
                }
            }
        }
        return  userTasks;
    }

    public List<GrcUserTask> getNextUserTask(String activityId){
        List<GrcUserTask> nextElement = getNextElement(activityId);
        return nextElement;

    }


    public List<GrcUserTask> getNextElement(String activityId){
        LinkedList<GrcUserTask> userTasks = new LinkedList<>();
        UserTask userTask = getUserTaskByActivityId(activityId);
        List<SequenceFlow> outgoingFlows = userTask.getOutgoingFlows();
        for (SequenceFlow outgoingFlow : outgoingFlows) {
            LinkedList<SequenceFlow> lineList = new LinkedList<SequenceFlow>();
            manageFlowElement(outgoingFlow,userTasks,lineList);
        }
        return  userTasks;
    }

    /**
     * 递归调用 寻找 userTasks
     *
     * @param userTasks
     */
    public  void manageFlowElement (SequenceFlow outgoingFlow, LinkedList<GrcUserTask> userTasks ,LinkedList<SequenceFlow> lineList ){
        lineList.add(outgoingFlow);
        FlowElement flowElement= outgoingFlow.getTargetFlowElement();

        if (flowElement instanceof UserTask) {

            userTasks.add( new GrcUserTask(
                    (UserTask) flowElement ,outgoingFlow,lineList
            ));
        } else  if (flowElement instanceof Gateway ) {
            List<SequenceFlow> outgoingFlows1 = ((Gateway) flowElement).getOutgoingFlows();
            for (SequenceFlow sequenceFlow : outgoingFlows1) {
                manageFlowElement(sequenceFlow,userTasks,lineList);
            }

        }
    }



    public void setBpmnModel(BpmnModel bpmnModel) {
        this.bpmnModel = bpmnModel;
    }

    public void setProcess(Process process) {
        this.process = process;
    }

    public void setLineList(LinkedList<SequenceFlow> lineList) {
        this.lineList = lineList;
    }

    public void setUserList(LinkedList<UserTask> userList) {
        this.userList = userList;
    }

    public void setGatewayList(LinkedList<Gateway> gatewayList) {
        this.gatewayList = gatewayList;
    }

    public void setStartList(LinkedList<StartEvent> startList) {
        this.startList = startList;
    }

    public void setEndList(LinkedList<EndEvent> endList) {
        this.endList = endList;
    }

    public BpmnModel getBpmnModel() {
        return bpmnModel;
    }

    public Process getProcess() {
        return process;
    }

    public LinkedList<SequenceFlow> getLineList() {
        return lineList;
    }

    public LinkedList<UserTask> getUserList() {
        return userList;
    }

    public LinkedList<Gateway> getGatewayList() {
        return gatewayList;
    }

    public LinkedList<StartEvent> getStartList() {
        return startList;
    }

    public LinkedList<EndEvent> getEndList() {
        return endList;
    }

    @Override
    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        lineListStr(stringBuffer);
        userListStr(stringBuffer);
        gatewayListStr(stringBuffer);
        endListListStr(stringBuffer);
        return  stringBuffer.toString();

    }

    private void endListListStr(StringBuffer stringBuffer) {
        endList.forEach(endEvent ->
                ActivitiFormateUtils.formatEndEvent(endEvent,stringBuffer)
        );
    }

    private void gatewayListStr(StringBuffer stringBuffer) {
        gatewayList.forEach(gateway ->
                ActivitiFormateUtils.formateGateway(gateway,stringBuffer)
        );
    }

    private void userListStr(StringBuffer stringBuffer) {


        userList.forEach(userTask ->
                ActivitiFormateUtils.formateUserTask(userTask,stringBuffer)

        );
    }

    private void lineListStr(StringBuffer stringBuffer) {

        System.out.println("Size:"+ lineList.size());
        lineList.forEach(sequenceFlow ->
                ActivitiFormateUtils.formateSequenceFlow(sequenceFlow,stringBuffer)
        );
    }
}
