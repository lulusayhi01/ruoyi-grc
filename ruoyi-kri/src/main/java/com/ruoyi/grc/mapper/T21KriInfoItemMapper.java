package com.ruoyi.grc.mapper;

import java.util.List;
import com.ruoyi.grc.domain.T21KriInfoItem;

/**
 * KRI指标明细Mapper接口
 * 
 * @author liuqi
 * @date 2021-01-23
 */
public interface T21KriInfoItemMapper 
{
    /**
     * 查询KRI指标明细
     * 
     * @param id KRI指标明细ID
     * @return KRI指标明细
     */
    public T21KriInfoItem selectT21KriInfoItemById(Long id);

    /**
     * 查询KRI指标明细列表
     * 
     * @param t21KriInfoItem KRI指标明细
     * @return KRI指标明细集合
     */
    public List<T21KriInfoItem> selectT21KriInfoItemList(T21KriInfoItem t21KriInfoItem);

    /**
     * 新增KRI指标明细
     * 
     * @param t21KriInfoItem KRI指标明细
     * @return 结果
     */
    public int insertT21KriInfoItem(T21KriInfoItem t21KriInfoItem);

    /**
     * 修改KRI指标明细
     * 
     * @param t21KriInfoItem KRI指标明细
     * @return 结果
     */
    public int updateT21KriInfoItem(T21KriInfoItem t21KriInfoItem);

    /**
     * 删除KRI指标明细
     * 
     * @param id KRI指标明细ID
     * @return 结果
     */
    public int deleteT21KriInfoItemById(Long id);

    /**
     * 批量删除KRI指标明细
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteT21KriInfoItemByIds(Long[] ids);
}
