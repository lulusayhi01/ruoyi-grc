package com.ruoyi.grc.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.util.ArrayUtil;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.grc.domain.LosseventAreaRelation;
import com.ruoyi.grc.domain.LosseventDeptRelation;
import com.ruoyi.grc.mapper.LosseventAreaRelationMapper;
import com.ruoyi.grc.mapper.LosseventDeptRelationMapper;
import com.ruoyi.grc.utils.SeqUtils;
import com.ruoyi.system.domain.SysUserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.grc.mapper.T21LdcLosseventMapper;
import com.ruoyi.grc.domain.T21LdcLossevent;
import com.ruoyi.grc.service.IT21LdcLosseventService;

/**
 * 损失事件Service业务层处理
 * 
 * @author liuqi
 * @date 2021-01-17
 */
@Service
public class T21LdcLosseventServiceImpl implements IT21LdcLosseventService 
{
    @Autowired
    private T21LdcLosseventMapper t21LdcLosseventMapper;

    @Autowired
    private LosseventAreaRelationMapper losseventAreaRelationMapper;

    @Autowired
    private LosseventDeptRelationMapper losseventDeptRelationMapper;
    /**
     * 查询损失事件
     * 
     * @param id 损失事件ID
     * @return 损失事件
     */
    @Override
    public T21LdcLossevent selectT21LdcLosseventById(Long id)
    {
        T21LdcLossevent t21LdcLossevent = t21LdcLosseventMapper.selectT21LdcLosseventById(id);

        List<Long> longs = losseventAreaRelationMapper.selectAreaIdListByLosseventId(id);
        Long[] deptIds = ArrayUtil.toArray(losseventDeptRelationMapper.selectLosseventDeptRelationBylosseventId(id).stream()
                .map(losseventDeptRelation -> losseventDeptRelation.getDeptId()).collect(Collectors.toList()), Long.class);

        t21LdcLossevent.setAreaIds(ArrayUtil.toArray(longs, Long.class));

        t21LdcLossevent.setDeptIds(deptIds);
        return t21LdcLossevent;
    }

    /**
     * 查询损失事件列表
     * 
     * @param t21LdcLossevent 损失事件
     * @return 损失事件
     */
    @Override
    public List<T21LdcLossevent> selectT21LdcLosseventList(T21LdcLossevent t21LdcLossevent)
    {
        return t21LdcLosseventMapper.selectT21LdcLosseventList(t21LdcLossevent);
    }

    /**
     * 新增损失事件
     * 
     * @param t21LdcLossevent 损失事件
     * @return 结果
     */
    @Override
    public int insertT21LdcLossevent(T21LdcLossevent t21LdcLossevent)
    {
        t21LdcLossevent.setCreateTime(DateUtils.getNowDate());
        t21LdcLossevent.setCode(SeqUtils.getNextCode("T002"));
        int i = t21LdcLosseventMapper.insertT21LdcLossevent(t21LdcLossevent);

        insertLosseventRelation(t21LdcLossevent);
        return i;
    }






    /**
     * 修改损失事件
     * 
     * @param t21LdcLossevent 损失事件
     * @return 结果
     */
    @Override
    public int updateT21LdcLossevent(T21LdcLossevent t21LdcLossevent)
    {
        t21LdcLossevent.setUpdateTime(DateUtils.getNowDate());
        updateLosseventAreaRelation(t21LdcLossevent);
        int i = t21LdcLosseventMapper.updateT21LdcLossevent(t21LdcLossevent);
        return i;
    }

    /**
     * 批量删除损失事件
     * 
     * @param ids 需要删除的损失事件ID
     * @return 结果
     */
    @Override
    public int deleteT21LdcLosseventByIds(Long[] ids)
    {
        return t21LdcLosseventMapper.deleteT21LdcLosseventByIds(ids);
    }

    /**
     * 删除损失事件信息
     * 
     * @param id 损失事件ID
     * @return 结果
     */
    @Override
    public int deleteT21LdcLosseventById(Long id)
    {
        return t21LdcLosseventMapper.deleteT21LdcLosseventById(id);
    }


    /**
     * 更新关系表-区域
     * @param t21LdcLossevent
     */
    private void updateLosseventAreaRelation(T21LdcLossevent t21LdcLossevent){
        // 删除
        deleteLosseventRelation(t21LdcLossevent);
        //插入
        insertLosseventRelation(t21LdcLossevent);
    }

    /**
     * 删除关系表
     * @param t21LdcLossevent
     */
    private void deleteLosseventRelation(T21LdcLossevent t21LdcLossevent) {
        losseventAreaRelationMapper.deleteAreaIdByLosseventId(t21LdcLossevent.getId());
        losseventDeptRelationMapper.deleteAreaIdBylosseventId(t21LdcLossevent.getId());
    }

    /**
     * 插入关系表
     * @param t21LdcLossevent
     */
    private void insertLosseventRelation(T21LdcLossevent t21LdcLossevent) {
        doLosseventAreaRelationInsert(t21LdcLossevent.getId(),t21LdcLossevent.getAreaIds());
        doLosseventDeptRelationInsert(t21LdcLossevent.getId(),t21LdcLossevent.getDeptIds());
    }

    private void doLosseventAreaRelationInsert(Long losseventId, Long[] areaIds) {
        if (StringUtils.isNotNull(areaIds))
        {
            List<LosseventAreaRelation> list = new ArrayList<LosseventAreaRelation>();
            for (Long areaId : areaIds)
            {
                LosseventAreaRelation relation = new LosseventAreaRelation();
                relation.setAreaId(areaId);
                relation.setLosseventId(losseventId);
                list.add(relation);
            }
            if (list.size() > 0)
            {
                losseventAreaRelationMapper.batchLosseventArea(list);
            }
        }
    }



    private void doLosseventDeptRelationInsert(Long losseventId, Long[] relationIds) {
        if (StringUtils.isNotNull(relationIds))
        {

            List<LosseventDeptRelation> list = new ArrayList<LosseventDeptRelation>();
            for (Long relationId : relationIds)
            {
                LosseventDeptRelation relation = new LosseventDeptRelation();
                relation.setLosseventId(losseventId);
                relation.setDeptId(relationId);
                list.add(relation);
            }
            if (list.size() > 0)
            {
                losseventDeptRelationMapper.batchLosseventDeptRelation(list);
            }
        }
    }

}
