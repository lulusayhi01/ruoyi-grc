package com.ruoyi;

import com.googlecode.aviator.AviatorEvaluator;
import com.googlecode.aviator.Expression;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.FlowNode;
import org.activiti.bpmn.model.SequenceFlow;
import org.activiti.engine.*;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricActivityInstanceQuery;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricVariableInstance;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

public class ActivitiTaskTest {
    public static String definitionKey="lossevent_6";
    ProcessEngine processEngine = null;
    RuntimeService runtimeService=null;

    @Before
    public void before() {
        //提前加载这个
        Expression compiledExp = AviatorEvaluator.compile("a>1");
        //创建ProcessEngineConfiguration对象
        ProcessEngineConfiguration configuration = ProcessEngineConfiguration.createProcessEngineConfigurationFromResource("activiti-cfg.xml");
        processEngine = configuration.buildProcessEngine();
        //获取RuntimeService对象
        runtimeService = processEngine.getRuntimeService();


    }

    @After
    public void after() {
        if (processEngine != null) {
            processEngine.close();
        }
    }



    @Test
    public void testQuery() {
        //获取TaskService对象
        TaskService taskService = processEngine.getTaskService();
        List<Task> taskList = taskService.createTaskQuery()
               // .taskId("d148ada0-4cfd-11eb-a3ca-b0359fadfbca")
                //.executionId("160001")
                .processInstanceId("162501")
                // .processDefinitionKey(definitionKey)
                //.processDefinitionId("lossevent_6:4:120003")
                // .taskCandidateUser("")
                //.taskAssignee("李四")
                // .taskCandidateUser("")  //组任务
                .list();
        System.out.println(taskList.size());
        //遍历任务列表
        for (Task task : taskList) {
           // System.out.println(ActivitiFormateUtils.formateTask(task,null));
           // GrcPocess grcPocess = ActivitiUtils.pocessALL(processEngine.getRepositoryService(), task.getProcessDefinitionId());

            //ActivitiUtils.resolvTask(processEngine,task);

            System.out.println(task.getName());
            taskService.complete(task.getId());
            /*
            FlowNode flowNode = ActivitiUtils.activFlowNode(processEngine, task);
            //记录原活动方向
            List<SequenceFlow> oriSequenceFlows = new ArrayList<SequenceFlow>();
            oriSequenceFlows.addAll(flowNode.getOutgoingFlows());

            //清理活动方向
            flowNode.getOutgoingFlows().clear();
            //建立新方向
            List<SequenceFlow> newSequenceFlowList = new ArrayList<SequenceFlow>();
            SequenceFlow newSequenceFlow = new SequenceFlow();
            newSequenceFlow.setId("newSequenceFlowId");
            newSequenceFlow.setSourceFlowElement(flowNode);
            newSequenceFlow.setTargetFlowElement(grcPocess.getUserTaskByActivityId("Activity_0dxrggh"));
            newSequenceFlowList.add(newSequenceFlow);
            flowNode.setOutgoingFlows(newSequenceFlowList);
            Map<String, Object> variables = new HashMap<>();
            variables.put("user", "张三");

            //办理
           // taskService.complete(task.getId(),variables);
            //拾取
            // taskService.claim(taskId,candidateUser);
            // 归还
           // taskService.setAssignee(taskId, null);
            //重置
            flowNode.setOutgoingFlows(oriSequenceFlows);*/






        }

    }




}
