package com.ruoyi.grc.mapper;

import java.util.List;
import com.ruoyi.grc.domain.T21KriInfo;

/**
 * KRI指标信息Mapper接口
 * 
 * @author liuqi
 * @date 2021-01-22
 */
public interface T21KriInfoMapper 
{
    /**
     * 查询KRI指标信息
     * 
     * @param id KRI指标信息ID
     * @return KRI指标信息
     */
    public T21KriInfo selectT21KriInfoById(Long id);




    /**
     * 查询KRI指标信息列表
     * 
     * @param t21KriInfo KRI指标信息
     * @return KRI指标信息集合
     */
    public List<T21KriInfo> selectT21KriInfoList(T21KriInfo t21KriInfo);

    /**
     * 新增KRI指标信息
     * 
     * @param t21KriInfo KRI指标信息
     * @return 结果
     */
    public int insertT21KriInfo(T21KriInfo t21KriInfo);

    /**
     * 修改KRI指标信息
     * 
     * @param t21KriInfo KRI指标信息
     * @return 结果
     */
    public int updateT21KriInfo(T21KriInfo t21KriInfo);

    /**
     * 删除KRI指标信息
     * 
     * @param id KRI指标信息ID
     * @return 结果
     */
    public int deleteT21KriInfoById(Long id);

    /**
     * 批量删除KRI指标信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteT21KriInfoByIds(Long[] ids);
}
