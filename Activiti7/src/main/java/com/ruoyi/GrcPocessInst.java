package com.ruoyi;

import org.activiti.bpmn.model.Process;
import org.activiti.bpmn.model.*;
import org.activiti.engine.history.HistoricActivityInstance;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class GrcPocessInst {
    private GrcPocess grcPocess=null;
    //全部历史
    private List<HistoricActivityInstance> ALLfinishedlist =null;

    //当前的节点
    private HistoricActivityInstance  thisActivity=  null;

    public List<GrcUserTask> getPreGrcUserTaskList(){
        String activityId = thisActivity.getActivityId();
        List<GrcUserTask> preUserTask = grcPocess.getPreUserTask(activityId);
        for (GrcUserTask grcUserTask : preUserTask) {
            //倒叙
            for (int i = ALLfinishedlist.size() - 1; i >= 0; i--) {
                HistoricActivityInstance historicActivityInstance = ALLfinishedlist.get(i);
                if(grcUserTask.getUserTask().getId().equals(historicActivityInstance.getActivityId())){
                    grcUserTask.setAssignee(historicActivityInstance.getAssignee());
                    break;
                }
            }
        }
        List<GrcUserTask> collect = preUserTask.stream().filter(grcUserTask -> StringUtils.isNotEmpty(grcUserTask.getAssignee())).collect(Collectors.toList());
        return  collect;
    }


    public GrcPocessInst(GrcPocess grcPocess, List<HistoricActivityInstance> ALLfinishedlist, HistoricActivityInstance thisActivity) {
        this.grcPocess = grcPocess;
        this.ALLfinishedlist = ALLfinishedlist;
        this.thisActivity = thisActivity;
    }

    public GrcPocessInst() {
    }

    public GrcPocess getGrcPocess() {
        return grcPocess;
    }

    public List<HistoricActivityInstance> getALLfinishedlist() {
        return ALLfinishedlist;
    }

    public HistoricActivityInstance getThisActivity() {
        return thisActivity;
    }

    public void setGrcPocess(GrcPocess grcPocess) {
        this.grcPocess = grcPocess;
    }

    public void setALLfinishedlist(List<HistoricActivityInstance> ALLfinishedlist) {
        this.ALLfinishedlist = ALLfinishedlist;
    }

    public void setThisActivity(HistoricActivityInstance thisActivity) {
        this.thisActivity = thisActivity;
    }
}
