package com.ruoyi.activiti.service;

import java.util.List;
import com.ruoyi.activiti.domain.ActGrcPocess;
import com.ruoyi.activiti.utils.GrcUserTask;

/**
 * 流程Service接口
 * 
 * @author ruoyi
 * @date 2020-12-05
 */
public interface IActGrcPocessService 
{


    /**
     * 查询流程
     * 
     * @param pocessId 流程ID
     * @return 流程
     */
    public ActGrcPocess selectActGrcPocessById(Long pocessId);

    /**
     * 查询流程列表
     * 
     * @param actGrcPocess 流程
     * @return 流程集合
     */
    public List<ActGrcPocess> selectActGrcPocessList(ActGrcPocess actGrcPocess);

    /**
     * 新增流程
     * 
     * @param actGrcPocess 流程
     * @return 结果
     */
    public int insertActGrcPocess(ActGrcPocess actGrcPocess);

    /**
     * 修改流程
     * 
     * @param actGrcPocess 流程
     * @return 结果
     */
    public int updateActGrcPocess(ActGrcPocess actGrcPocess);

    /**
     * 批量删除流程
     * 
     * @param pocessIds 需要删除的流程ID
     * @return 结果
     */
    public int deleteActGrcPocessByIds(Long[] pocessIds);

    /**
     * 删除流程信息
     * 
     * @param pocessId 流程ID
     * @return 结果
     */
    public int deleteActGrcPocessById(Long pocessId);
}
