package com.ruoyi.grc.service;

import java.util.List;
import com.ruoyi.grc.domain.T00FileInfo;

/**
 * 附件信息Service接口
 * 
 * @author liuqi
 * @date 2021-01-24
 */
public interface IT00FileInfoService 
{
    /**
     * 查询附件信息
     * 
     * @param fileId 附件信息ID
     * @return 附件信息
     */
    public T00FileInfo selectT00FileInfoById(Long fileId);

    /**
     * 查询附件信息列表
     * 
     * @param t00FileInfo 附件信息
     * @return 附件信息集合
     */
    public List<T00FileInfo> selectT00FileInfoList(T00FileInfo t00FileInfo);

    /**
     * 新增附件信息
     * 
     * @param t00FileInfo 附件信息
     * @return 结果
     */
    public int insertT00FileInfo(T00FileInfo t00FileInfo);

    /**
     * 修改附件信息
     * 
     * @param t00FileInfo 附件信息
     * @return 结果
     */
    public int updateT00FileInfo(T00FileInfo t00FileInfo);

    /**
     * 批量删除附件信息
     * 
     * @param fileIds 需要删除的附件信息ID
     * @return 结果
     */
    public int deleteT00FileInfoByIds(Long[] fileIds);

    /**
     * 删除附件信息信息
     * 
     * @param fileId 附件信息ID
     * @return 结果
     */
    public int deleteT00FileInfoById(Long fileId);



}
