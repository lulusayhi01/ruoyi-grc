package com.ruoyi.grc.cach;
import java.util.List;

public interface ICachService<T> {
    public List<T> getCachDBList();
}
