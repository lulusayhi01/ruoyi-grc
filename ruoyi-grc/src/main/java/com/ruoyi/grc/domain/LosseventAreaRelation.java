package com.ruoyi.grc.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 损失事件对象 t21_ldc_lossevent
 * 
 * @author liuqi
 * @date 2021-01-17
 */
public class LosseventAreaRelation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long losseventId;
    /** ID */
    private Long areaId;

    public Long getLosseventId() {
        return losseventId;
    }

    public void setLosseventId(Long losseventId) {
        this.losseventId = losseventId;
    }

    public Long getAreaId() {
        return areaId;
    }

    public void setAreaId(Long areaId) {
        this.areaId = areaId;
    }
}
