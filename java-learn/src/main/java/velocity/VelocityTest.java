package velocity;

import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.utils.StringUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

public class VelocityTest {

    public static void main(String[] args) {


        initVelocity();

        System.out.println(StringUtils.capitalize("lossevent"));

        // 渲染模板
        StringWriter sw = new StringWriter();
        VelocityContext velocityContext = new VelocityContext();
        velocityContext.put("tableName", "表名称");

        ArrayList<String> columns = new ArrayList<>();
        columns.add("column1");
        columns.add("column2");
        columns.add("column3");
        columns.add("column4");
        columns.add("column5");
        velocityContext.put("columns", columns);

        velocityContext.put("query", false);

        columns.size();


        HashMap<String, Object> mapbean = new HashMap<>();

        mapbean.put("isok",true);
        mapbean.put("name","张三");
        velocityContext.put("dateMap", mapbean);


        VelocityBean dateBean = new VelocityBean();

        velocityContext.put("dateBean", dateBean);

        Template tpl = Velocity.getTemplate("vm/hellovelocity.vm", Constants.UTF8);


        tpl.merge(velocityContext, sw);
        System.out.println(sw.toString());

    }

    /**
     * 初始化vm方法
     */
    public static void initVelocity()
    {
        Properties p = new Properties();
        try
        {
            // 加载classpath目录下的vm文件
            p.setProperty("file.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
            // 定义字符集
            p.setProperty(Velocity.ENCODING_DEFAULT, Constants.UTF8);
            p.setProperty(Velocity.OUTPUT_ENCODING, Constants.UTF8);
            // 初始化Velocity引擎，指定配置Properties
            Velocity.init(p);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

}
