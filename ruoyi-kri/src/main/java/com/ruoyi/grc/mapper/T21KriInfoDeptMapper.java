package com.ruoyi.grc.mapper;

import java.util.List;
import com.ruoyi.grc.domain.T21KriInfoDept;

/**
 * KRI指标关联部门Mapper接口
 * 
 * @author liuqi
 * @date 2021-01-22
 */
public interface T21KriInfoDeptMapper 
{


    /**

     BaseEntity 添加
     private Long[] deptIds;
     private String[] deptIds;

     // ServiceImpl 添加
     @Autowired
     private T21KriInfoDeptMapper t21KriInfoDeptMapper;

     //新增
     private void doT21KriInfoDeptInsert(Long majorId, Long[] relationIds) {
            if (StringUtils.isNotNull(relationIds))
            {
                List<T21KriInfoDept> list = new ArrayList<T21KriInfoDept>();
                for (Long relationId : relationIds)
                {
                                        T21KriInfoDept relation = new T21KriInfoDept();
                     relation.setMajorId(majorId);
                     relation.setDeptId(relationId);
                     list.add(relation);
                }
                if (list.size() > 0)
                {
                    t21KriInfoDeptMapper.batchT21KriInfoDept(list);
                }
            }
        }
        //删除
         t21KriInfoDeptMapper.deleteAreaIdBymajorId(majorId);

        //查询
        List<T21KriInfoDept> t21KriInfoDeptList =  t21KriInfoDeptMapper.selectT21KriInfoDeptBymajorId(majorId);



          Long[] t21KriInfoDeptIds = ArrayUtil.toArray(t21KriInfoDeptMapper.selectT21KriInfoDeptBymajorId(majorId).stream()
         .map(t21KriInfoDept -> t21KriInfoDept.getDeptId()).collect(Collectors.toList()), Long.class);


      * **/

    /**
    * 批量新增
    * @param list
    * @return
    */
    public int batchT21KriInfoDept(List<T21KriInfoDept> list);

    /**
     * 查询关联
     * @param majorId
     * @return
     */
    public List<T21KriInfoDept> selectT21KriInfoDeptBymajorId(Long majorId);

    /**
     * 删除
     * @param majorId
     * @return
     */
    public int deleteAreaIdBymajorId(Long majorId);

}
