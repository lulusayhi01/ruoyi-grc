package com.ruoyi.grc.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.grc.domain.T00FileItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.grc.mapper.T00FileInfoMapper;
import com.ruoyi.grc.domain.T00FileInfo;
import com.ruoyi.grc.service.IT00FileInfoService;

/**
 * 附件信息Service业务层处理
 * 
 * @author liuqi
 * @date 2021-01-24
 */
@Service
public class T00FileInfoServiceImpl implements IT00FileInfoService 
{
    @Autowired
    private T00FileInfoMapper t00FileInfoMapper;

    /**
     * 查询附件信息
     * 
     * @param fileId 附件信息ID
     * @return 附件信息
     */
    @Override
    public T00FileInfo selectT00FileInfoById(Long fileId)
    {


        T00FileInfo t00FileInfo=   t00FileInfoMapper.selectT00FileInfoById(fileId);
        doSelectRelationes(t00FileInfo);

        return t00FileInfo;
    }

    /**
     * 查询附件信息列表
     * 
     * @param t00FileInfo 附件信息
     * @return 附件信息
     */
    @Override
    public List<T00FileInfo> selectT00FileInfoList(T00FileInfo t00FileInfo)
    {
        return t00FileInfoMapper.selectT00FileInfoList(t00FileInfo);
    }

    /**
     * 新增附件信息
     * 
     * @param t00FileInfo 附件信息
     * @return 结果
     */
    @Override
    public int insertT00FileInfo(T00FileInfo t00FileInfo)
    {
        t00FileInfo.setCreateTime(DateUtils.getNowDate());
        //编号初始
        //t00FileInfo.setCode(SeqUtils.getNextCode("T000xx"));

        doInsertRelationes(t00FileInfo);

        List<T00FileItem> fileItemList = t00FileInfo.getFileItemList();

        for (T00FileItem t00FileItem : fileItemList) {
            t00FileInfo.setFileName(t00FileItem.getFileName());
            t00FileInfo.setFilePath(t00FileItem.getFilePath());
            t00FileInfo.setFileSize(t00FileItem.getFileSize());
            t00FileInfoMapper.insertT00FileInfo(t00FileInfo);
        }
        return 1;
    }

    /**
     * 修改附件信息
     * 
     * @param t00FileInfo 附件信息
     * @return 结果
     */
    @Override
    public int updateT00FileInfo(T00FileInfo t00FileInfo)
    {
        doUpdateRelation(t00FileInfo);
        return t00FileInfoMapper.updateT00FileInfo(t00FileInfo);
    }

    /**
     * 批量删除附件信息
     * 
     * @param fileIds 需要删除的附件信息ID
     * @return 结果
     */
    @Override
    public int deleteT00FileInfoByIds(Long[] fileIds)
    {
        return t00FileInfoMapper.deleteT00FileInfoByIds(fileIds);
    }

    /**
     * 删除附件信息信息
     * 
     * @param fileId 附件信息ID
     * @return 结果
     */
    @Override
    public int deleteT00FileInfoById(Long fileId)
    {
        doDeleteRelationes(fileId);
        return t00FileInfoMapper.deleteT00FileInfoById(fileId);
    }


    /**
    * 查询关系表表
    * @param t00FileInfo
    */
    private void doSelectRelationes(T00FileInfo t00FileInfo) {
        
        Long  fileId = t00FileInfo.getFileId();


    }

    /**
    * 更新关系表表
    * @param t00FileInfo
    */
    private void doUpdateRelation(T00FileInfo t00FileInfo){

                Long  fileId = t00FileInfo.getFileId();
        // 删除
        doDeleteRelationes(fileId);
        //插入
        doInsertRelationes(t00FileInfo);
    }

    /**
     * 删除关系表
     * @param fileId
     */
    private void doDeleteRelationes(Long fileId) {
        // t00FileInfoRelationMapper.deleteAreaIdById(fileId);
    }

    /**
     * 插入关系表
     * @param t00FileInfo
     */
    private void doInsertRelationes(T00FileInfo t00FileInfo) {
        //doT00FileInfoRelationInsert(t00FileInfo.getId(),t00FileInfo.getXxxxIds());

    }

}
