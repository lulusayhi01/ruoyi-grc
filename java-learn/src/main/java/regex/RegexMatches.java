package regex;

import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexMatches {

    private static final String REGEX = "\\bcat\\b";
    private static final String INPUT =
            "cat cat cat cattie cat";
    @Test
    public void testGroup() {
        {
            String aa = "aa\"\\()";
            System.out.println(aa);
            String strE = "mSurface=  Surf=\"ace(name=com.bbk.launcher2/com.bbk.launcher2.Launcher)";
            // String pattern = "[\\u4E00-\\u9FA5]+";
            //"(\\()([0-9a-zA-Z\\.\\/\\=])*(\\))")
            String pattern = "(Surf=\"ace\\()(.)*\\)";
            System.out.println(pattern);
            Pattern r = Pattern.compile(pattern);
            Matcher m = r.matcher(strE);

            while (m.find()) {
                System.out.println("start(): " + m.start());
                System.out.println("end(): " + m.end());
                System.out.println("Found value: " + m.group(0));
                System.out.println("Found value: " + m.group(1));
            }
        }
    }

    @Test
    public void testGroup2() {
        String line = "ab#11#u#222#v";
        String pattern = "#(.*?)#";
        // 创建 Pattern 对象
        Pattern r = Pattern.compile(pattern);
        // 现在创建 matcher 对象
        Matcher m = r.matcher(line);
        while (m.find()) {
            int count = m.groupCount();
            System.out.println(count);
            for (int i = 0; i <=count ; i++) {
                System.out.println("Found value: " + m.group(i) );
            }
        }

    }


}
