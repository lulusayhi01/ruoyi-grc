package com.ruoyi.grc.mapper;

import java.util.List;
import com.ruoyi.grc.domain.T01Seq;

/**
 * 编号生成Mapper接口
 * 
 * @author liuqi
 * @date 2021-01-15
 */
public interface T01SeqMapper 
{
    /**
     * 查询编号生成
     * 
     * @param id 编号生成ID
     * @return 编号生成
     */
    public T01Seq selectT01SeqById(Long id);

    /**
     * 查询编号生成列表
     * 
     * @param t01Seq 编号生成
     * @return 编号生成集合
     */
    public List<T01Seq> selectT01SeqList(T01Seq t01Seq);

    /**
     * 新增编号生成
     * 
     * @param t01Seq 编号生成
     * @return 结果
     */
    public int insertT01Seq(T01Seq t01Seq);

    /**
     * 修改编号生成
     * 
     * @param t01Seq 编号生成
     * @return 结果
     */
    public int updateT01Seq(T01Seq t01Seq);

    /**
     * 删除编号生成
     * 
     * @param id 编号生成ID
     * @return 结果
     */
    public int deleteT01SeqById(Long id);

    /**
     * 批量删除编号生成
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteT01SeqByIds(Long[] ids);
}
