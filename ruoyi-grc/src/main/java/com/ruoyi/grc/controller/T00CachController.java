package com.ruoyi.grc.controller;

import java.util.List;
import java.util.Map;

import cn.hutool.extra.spring.SpringUtil;
import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.grc.cach.CachUtils;
import com.ruoyi.grc.cach.ICachService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.grc.domain.T00Cach;
import com.ruoyi.grc.service.IT00CachService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 缓存sqlController
 * 
 * @author liuqi
 * @date 2021-01-19
 */
@RestController
@RequestMapping("/grc/cach")
public class T00CachController extends BaseController
{
    @Autowired
    private IT00CachService t00CachService;


    /**
     * 查询缓存sql列表
     */
    @PreAuthorize("@ss.hasPermi('grc:cach:list')")
    @GetMapping(value = "/listDate/{beanName}")
    public AjaxResult listDate(@PathVariable("beanName") String beanName)
    {

        List cacheList = CachUtils.getCacheList(beanName);
        return AjaxResult.success(cacheList);
    }

    /**
     * 查询缓存sql列表
     */
    @PreAuthorize("@ss.hasPermi('grc:cach:list')")
    @GetMapping(value = "/clearCache/{beanName}")
    public AjaxResult clearCache(@PathVariable("beanName") String beanName)
    {
        long l = CachUtils.clearCache(beanName);

        return AjaxResult.success(String.format("缓存名称 : %s  清理了  %s  条数据"
                ,beanName
                ,l
        )
        );
    }

    /**
     * 查询缓存sql列表
     */
    @PreAuthorize("@ss.hasPermi('grc:cach:list')")
    @GetMapping("/list")
    public TableDataInfo list(T00Cach t00Cach)
    {
        startPage();
        List<T00Cach> list = t00CachService.selectT00CachList(t00Cach);
        return getDataTable(list);
    }

    /**
     * 导出缓存sql列表
     */
    @PreAuthorize("@ss.hasPermi('grc:cach:export')")
    @Log(title = "缓存sql", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(T00Cach t00Cach)
    {
        List<T00Cach> list = t00CachService.selectT00CachList(t00Cach);
        ExcelUtil<T00Cach> util = new ExcelUtil<T00Cach>(T00Cach.class);
        return util.exportExcel(list, "cach");
    }

    /**
     * 获取缓存sql详细信息
     */
    @PreAuthorize("@ss.hasPermi('grc:cach:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(t00CachService.selectT00CachById(id));
    }

    /**
     * 新增缓存sql
     */
    @PreAuthorize("@ss.hasPermi('grc:cach:add')")
    @Log(title = "缓存sql", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody T00Cach t00Cach)
    {
        return toAjax(t00CachService.insertT00Cach(t00Cach));
    }

    /**
     * 修改缓存sql
     */
    @PreAuthorize("@ss.hasPermi('grc:cach:edit')")
    @Log(title = "缓存sql", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody T00Cach t00Cach)
    {
        return toAjax(t00CachService.updateT00Cach(t00Cach));
    }

    /**
     * 删除缓存sql
     */
    @PreAuthorize("@ss.hasPermi('grc:cach:remove')")
    @Log(title = "缓存sql", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(t00CachService.deleteT00CachByIds(ids));
    }
}
