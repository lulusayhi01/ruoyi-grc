package com.ruoyi.activiti.mapper;

import java.util.List;
import com.ruoyi.activiti.domain.ActGrcPocessVariables;

/**
 * 流程参数Mapper接口
 * 
 * @author ruoyi
 * @date 2020-12-06
 */
public interface ActGrcPocessVariablesMapper 
{
    /**
     * 查询流程参数
     * 
     * @param id 流程参数ID
     * @return 流程参数
     */
    public ActGrcPocessVariables selectActGrcPocessVariablesById(Long id);

    /**
     * 查询流程参数列表
     * 
     * @param actGrcPocessVariables 流程参数
     * @return 流程参数集合
     */
    public List<ActGrcPocessVariables> selectActGrcPocessVariablesList(ActGrcPocessVariables actGrcPocessVariables);

    /**
     * 新增流程参数
     * 
     * @param actGrcPocessVariables 流程参数
     * @return 结果
     */
    public int insertActGrcPocessVariables(ActGrcPocessVariables actGrcPocessVariables);

    /**
     * 修改流程参数
     * 
     * @param actGrcPocessVariables 流程参数
     * @return 结果
     */
    public int updateActGrcPocessVariables(ActGrcPocessVariables actGrcPocessVariables);

    /**
     * 删除流程参数
     * 
     * @param id 流程参数ID
     * @return 结果
     */
    public int deleteActGrcPocessVariablesById(Long id);

    /**
     * 批量删除流程参数
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteActGrcPocessVariablesByIds(Long[] ids);
}
