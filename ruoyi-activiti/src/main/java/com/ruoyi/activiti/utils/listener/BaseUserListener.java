package com.ruoyi.activiti.utils.listener;

import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import org.activiti.bpmn.model.ExtensionAttribute;
import org.activiti.bpmn.model.ExtensionElement;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.bpmn.model.SequenceFlow;
import org.activiti.engine.delegate.*;
import org.activiti.engine.impl.persistence.entity.ExecutionEntityImpl;
import org.activiti.engine.impl.persistence.entity.TaskEntityImpl;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public  abstract class BaseUserListener implements ExecutionListener, TaskListener {

   private static final Log log = LogFactory.get();

    @Override
    //ExecutionListener
    public void notify(DelegateExecution execution) {
        ExecutionEntityImpl executionEntity=  (ExecutionEntityImpl)execution;

        //事件名称
        String eventName = executionEntity.getEventName();

        //元素
        FlowElement currentFlowElement = executionEntity.getCurrentFlowElement();
        String activityId = currentFlowElement.getId();
        String activityName = currentFlowElement.getName();

        //实例
        ExecutionEntityImpl processInstance = executionEntity.getProcessInstance();

        String processDefinitionId = processInstance.getProcessDefinitionId();
        String processInstanceId = processInstance.getId();
        String businessKey = processInstance.getBusinessKey();

        Map<String, List<ExtensionElement>> extensionElements = currentFlowElement.getExtensionElements();
        List<ExtensionElement> properties = extensionElements.get("properties");

        HashMap<String, String> extensionPropertieMap = new HashMap<>();
        if(properties!= null){
            //获取扩展属性
            for (ExtensionElement property : properties) {
                Map<String, List<ExtensionElement>> childElements = property.getChildElements();
                for (Map.Entry<String, List<ExtensionElement>> stringListEntry : childElements.entrySet()) {
                    String key = stringListEntry.getKey();
                    List<ExtensionElement> value = stringListEntry.getValue();
                    for (ExtensionElement extensionElement : value) {


                        Map<String, List<ExtensionAttribute>> attributes = extensionElement.getAttributes();
                        String prop_name = attributes.get("name").get(0).getValue();
                        String prop_value = attributes.get("value").get(0).getValue();

                        extensionPropertieMap.put(prop_name,prop_value);

                    }
                }
            }
        }





        log.info("ExecutionListener-"+String.format(
                "  activityId : %s  activityName : %s   流程图id : %s  业务id : %s 事件名称 : %s     流程实例 : %s"
                ,activityId
                ,activityName
                ,processDefinitionId
                ,businessKey
                , eventName
                ,processInstanceId

        ));

        log.info("扩展属性："+extensionPropertieMap.toString());
        doUserTask(executionEntity,extensionPropertieMap,businessKey);

    }

    public abstract void doUserTask ( ExecutionEntityImpl executionEntity, HashMap<String, String> extensionPropertieMap ,String businessKey );

    @Override
    //TaskListener
    public void notify(DelegateTask delegateTask) {
        TaskEntityImpl taskEntity=(TaskEntityImpl)delegateTask;

        String name = taskEntity.getName();
        String eventName = taskEntity.getEventName();

        String assignee = taskEntity.getAssignee();
        String processInstanceBusinessKey = taskEntity.getProcessInstance().getProcessInstanceBusinessKey();
        log.info("------TaskListener-------"+String.format(

                "  name : %s  eventName : %s  assignee : %s  processInstanceBusinessKey : %s  "
                ,name
                , eventName
                , assignee
                ,processInstanceBusinessKey
        ));
    }



}
