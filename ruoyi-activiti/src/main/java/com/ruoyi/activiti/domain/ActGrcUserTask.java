package com.ruoyi.activiti.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 工作流用户节点对象 act_grc_user_task
 * 
 * @author ruoyi
 * @date 2020-12-06
 */
public class ActGrcUserTask extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 实例id */
    @Excel(name = "实例id")
    private String proInstanceId;

    /** 节点id */
    @Excel(name = "节点id")
    private String elementId;

    /** 节点名称 */
    @Excel(name = "节点名称")
    private String elementName;






    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setProInstanceId(String proInstanceId) 
    {
        this.proInstanceId = proInstanceId;
    }

    public String getProInstanceId() 
    {
        return proInstanceId;
    }
    public void setElementId(String elementId) 
    {
        this.elementId = elementId;
    }

    public String getElementId() 
    {
        return elementId;
    }
    public void setElementName(String elementName) 
    {
        this.elementName = elementName;
    }

    public String getElementName() 
    {
        return elementName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("proInstanceId", getProInstanceId())
            .append("elementId", getElementId())
            .append("elementName", getElementName())
            .toString();
    }
}
