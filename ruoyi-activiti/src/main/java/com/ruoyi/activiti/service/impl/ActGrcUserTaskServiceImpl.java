package com.ruoyi.activiti.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.activiti.mapper.ActGrcUserTaskMapper;
import com.ruoyi.activiti.domain.ActGrcUserTask;
import com.ruoyi.activiti.service.IActGrcUserTaskService;

/**
 * 工作流用户节点Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-12-06
 */
@Service
public class ActGrcUserTaskServiceImpl implements IActGrcUserTaskService 
{
    @Autowired
    private ActGrcUserTaskMapper actGrcUserTaskMapper;

    /**
     * 查询工作流用户节点
     * 
     * @param id 工作流用户节点ID
     * @return 工作流用户节点
     */
    @Override
    public ActGrcUserTask selectActGrcUserTaskById(Long id)
    {
        return actGrcUserTaskMapper.selectActGrcUserTaskById(id);
    }

    /**
     * 查询工作流用户节点列表
     * 
     * @param actGrcUserTask 工作流用户节点
     * @return 工作流用户节点
     */
    @Override
    public List<ActGrcUserTask> selectActGrcUserTaskList(ActGrcUserTask actGrcUserTask)
    {
        return actGrcUserTaskMapper.selectActGrcUserTaskList(actGrcUserTask);
    }

    /**
     * 新增工作流用户节点
     * 
     * @param actGrcUserTask 工作流用户节点
     * @return 结果
     */
    @Override
    public int insertActGrcUserTask(ActGrcUserTask actGrcUserTask)
    {
        return actGrcUserTaskMapper.insertActGrcUserTask(actGrcUserTask);
    }

    /**
     * 修改工作流用户节点
     * 
     * @param actGrcUserTask 工作流用户节点
     * @return 结果
     */
    @Override
    public int updateActGrcUserTask(ActGrcUserTask actGrcUserTask)
    {
        return actGrcUserTaskMapper.updateActGrcUserTask(actGrcUserTask);
    }

    /**
     * 批量删除工作流用户节点
     * 
     * @param ids 需要删除的工作流用户节点ID
     * @return 结果
     */
    @Override
    public int deleteActGrcUserTaskByIds(Long[] ids)
    {
        return actGrcUserTaskMapper.deleteActGrcUserTaskByIds(ids);
    }

    /**
     * 删除工作流用户节点信息
     * 
     * @param id 工作流用户节点ID
     * @return 结果
     */
    @Override
    public int deleteActGrcUserTaskById(Long id)
    {
        return actGrcUserTaskMapper.deleteActGrcUserTaskById(id);
    }
}
