package com.ruoyi.activiti.utils;

import org.activiti.bpmn.model.Gateway;
import org.activiti.bpmn.model.SequenceFlow;
import org.activiti.bpmn.model.UserTask;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;

public class ActivitiFormateUtils {

    public static  StringBuffer formateHistoricActivityInstance(HistoricActivityInstance hActivityInstance , StringBuffer stringBuffer ){
        if(stringBuffer ==null){
            stringBuffer=new StringBuffer();
        }
        return stringBuffer.append(String.format("hActivityInstance   Id :{%s}  TaskId :{%s}  Assignee: {%s} ActivityId ：{%s}  ActivityName：{%s} "
                ,hActivityInstance.getId()
                ,hActivityInstance.getTaskId()
                ,hActivityInstance.getAssignee()
                ,hActivityInstance.getActivityId()
                ,hActivityInstance.getActivityName()

        ));

    }
    public static  StringBuffer formateTask(Task task , StringBuffer stringBuffer ){
        if(stringBuffer ==null){
            stringBuffer=new StringBuffer();
        }
        stringBuffer.append(String.format("流程定义的id = %s "
                ,task.getProcessDefinitionId()

        ));
        stringBuffer.append("\r\n");
        stringBuffer.append((String.format("任务id = %s  任务名称 = %s   实例的id = %s  任务负责人= %s "
                ,task.getId()
                ,task.getName()
                ,task.getProcessInstanceId()
                ,task.getAssignee()

        )));
        stringBuffer.append("\r\n");
        return stringBuffer;
    }

    public static  StringBuffer formateProcessInstance(ProcessInstance processInstance , StringBuffer stringBuffer ){
        if(stringBuffer ==null){
            stringBuffer=new StringBuffer();
        }
        stringBuffer.append(String.format("流程定义的id =  %s 部署的id =  %s "
                ,processInstance.getProcessDefinitionId()
                ,processInstance.getDeploymentId()
        ));
        stringBuffer.append("\r\n");
        stringBuffer.append((String.format(" 实例的id =   %s  流程实例的 id =  %s 当前活动的id = %s "
                ,processInstance.getProcessInstanceId()
                ,processInstance.getId()
                ,processInstance.getActivityId()

        )));
        stringBuffer.append("\r\n");
        return stringBuffer;
    }

    public static  StringBuffer formateDeployment(Deployment deployment , StringBuffer stringBuffer ){
        if(stringBuffer ==null){
            stringBuffer=new StringBuffer();
        }
        stringBuffer.append(String.format("流程部署id = %s 流程部署名称 = %s "
                ,deployment.getId()
                , deployment.getName()

        ));
        stringBuffer.append("\r\n");

        return stringBuffer;
    }

    public static  StringBuffer formateProcessDefinition(ProcessDefinition processDefinition , StringBuffer stringBuffer ){
        if(stringBuffer ==null){
            stringBuffer=new StringBuffer();
        }
        stringBuffer.append(String.format("流程定义 id :{%s}   name:{%s}   key:{%s}   version: {%s} "
                ,processDefinition.getId()
                , processDefinition.getName()
                ,processDefinition.getKey()
                ,processDefinition.getVersion()
        ));
        stringBuffer.append("\r\n");

        return stringBuffer;
    }


    public static  StringBuffer formateGateway( Gateway  gateway , StringBuffer stringBuffer ){
        if(stringBuffer ==null){
            stringBuffer=new StringBuffer();
        }
       return stringBuffer.append(
                        String.format( "BaseElement= id :{%s}  "
                                ,gateway.getId()
                        ) +
                        String.format( "name :{%s}  "
                                ,gateway.getName()

                        ) +
                        String.format( "FlowNode=  InFlows :{%s} OutFlows :{%s} "
                                ,gateway.getIncomingFlows()
                                ,gateway.getOutgoingFlows()

                        ) +
                        String.format( "gateway= defaultFlow :{%s} "
                                ,gateway.getDefaultFlow()
                        )+"\r\n"
        );
    }

    public static  StringBuffer formateUserTask( UserTask  userTask , StringBuffer stringBuffer ){


        
        if(stringBuffer ==null){
            stringBuffer=new StringBuffer();
        }
       return stringBuffer.append(
                        String.format( "BaseElement= id :{%s}  "
                                ,userTask.getId()
                        ) +
                        String.format( "name :{%s}  "
                                ,userTask.getName()

                        ) +
                        String.format( "FlowNode=  InFlows :{%s} OutFlows :{%s} "
                                ,userTask.getIncomingFlows()
                                ,userTask.getOutgoingFlows()

                        ) +
                        String.format( "userTask= Assignee :{%s}  category : {%s}   skipExpression ：{%s}    "
                                ,userTask.getAssignee()
                                ,userTask.getCategory()
                                ,userTask.getSkipExpression()
                                //,userTask.getCustomUserIdentityLinks()
                                //,userTask.getCustomGroupIdentityLinks()
                                // ,userTask.getCustomProperties()
                                //customUserIdentityLinks  ：{%s}
                                //customGroupIdentityLinks  ：{%s}
                                //customProperties  ：{%s}
                                // ,userTask.getCandidateUsers()
                                // ,userTask.getCandidateGroups()
                        )+"\r\n"

        );
    }

    public static  StringBuffer formateSequenceFlow( SequenceFlow  sequenceFlow , StringBuffer stringBuffer ){
        if(stringBuffer ==null){
            stringBuffer=new StringBuffer();
        }

        return stringBuffer.append(
                                String.format( "BaseElement= id :{%s}  "
                                        ,sequenceFlow.getId()

                                ) +
                                String.format( " Name : {%s}  "
                                        ,sequenceFlow.getName()

                                ) +
                                String.format( "SequenceFlow= ref :{%s}    target: {%s}   Condition:  {%s}  "
                                        ,sequenceFlow.getSourceRef()
                                        ,sequenceFlow.getTargetRef()
                                        ,sequenceFlow.getConditionExpression()
                                        //,sequenceFlow.getSourceFlowElement()
                                        // ,sequenceFlow.getTargetFlowElement()
                                        //,sequenceFlow.getExecutionListeners()
                                        // ,sequenceFlow.getExtensionElements().keySet()

                                )+"\r\n"




        );
    }




}
