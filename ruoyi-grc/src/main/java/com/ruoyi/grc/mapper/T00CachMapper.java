package com.ruoyi.grc.mapper;

import java.util.List;
import com.ruoyi.grc.domain.T00Cach;

/**
 * 缓存sqlMapper接口
 * 
 * @author liuqi
 * @date 2021-01-19
 */
public interface T00CachMapper 
{
    /**
     * 查询缓存sql
     * 
     * @param id 缓存sqlID
     * @return 缓存sql
     */
    public T00Cach selectT00CachById(Long id);

    /**
     * 查询缓存sql列表
     * 
     * @param t00Cach 缓存sql
     * @return 缓存sql集合
     */
    public List<T00Cach> selectT00CachList(T00Cach t00Cach);

    /**
     * 新增缓存sql
     * 
     * @param t00Cach 缓存sql
     * @return 结果
     */
    public int insertT00Cach(T00Cach t00Cach);

    /**
     * 修改缓存sql
     * 
     * @param t00Cach 缓存sql
     * @return 结果
     */
    public int updateT00Cach(T00Cach t00Cach);

    /**
     * 删除缓存sql
     * 
     * @param id 缓存sqlID
     * @return 结果
     */
    public int deleteT00CachById(Long id);

    /**
     * 批量删除缓存sql
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteT00CachByIds(Long[] ids);
}
