package com.ruoyi.grc.mapper;

import java.util.List;
import com.ruoyi.grc.domain.LosseventDeptRelation;

/**
 * 损失事件+相关部门Mapper接口
 * 
 * @author liuqi
 * @date 2021-01-21
 */
public interface LosseventDeptRelationMapper 
{


    /**


     @Autowired
     private LosseventDeptRelationMapper losseventDeptRelationMapper;

     //新增
     private void doLosseventDeptRelationInsert(Long losseventId, Long[] relationIds) {
            if (StringUtils.isNotNull(relationIds))
            {
                // 新增用户与角色管理
                List<LosseventDeptRelation> list = new ArrayList<LosseventDeptRelation>();
                for (Long relationId : relationIds)
                {
                    LosseventDeptRelation relation = new LosseventDeptRelation();

                       relation.setLosseventId(losseventId);
                     relation.setDeptId(relationId);
                     list.add(relation);
                }
                if (list.size() > 0)
                {
                    losseventDeptRelationMapper.batchLosseventDeptRelation(list);
                }
            }
        }
        //删除
         losseventDeptRelationMapper.deleteAreaIdBylosseventId(losseventId);

        //查询
        List<LosseventDeptRelation> losseventDeptRelationList =  losseventDeptRelationMapper.selectLosseventDeptRelationBylosseventId(losseventId);



          Long[] losseventDeptRelationIds = ArrayUtil.toArray(losseventDeptRelationMapper.selectLosseventDeptRelationBylosseventId(losseventId).stream()
         .map(losseventDeptRelation -> losseventDeptRelation.getDeptId()).collect(Collectors.toList()), Long.class);


      * **/

    /**
    * 批量新增
    * @param list
    * @return
    */
    public int batchLosseventDeptRelation(List<LosseventDeptRelation> list);

    /**
     * 查询关联
     * @param losseventId
     * @return
     */
    public List<LosseventDeptRelation> selectLosseventDeptRelationBylosseventId(Long losseventId);

    /**
     * 删除
     * @param losseventId
     * @return
     */
    public int deleteAreaIdBylosseventId(Long losseventId);

}
