package com.ruoyi.activiti.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.activiti.mapper.ActGrcInstanceHistoricMapper;
import com.ruoyi.activiti.domain.ActGrcInstanceHistoric;
import com.ruoyi.activiti.service.IActGrcInstanceHistoricService;

/**
 *  流程历史Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-12-07
 */
@Service
public class ActGrcInstanceHistoricServiceImpl implements IActGrcInstanceHistoricService 
{
    @Autowired
    private ActGrcInstanceHistoricMapper actGrcInstanceHistoricMapper;

    /**
     * 查询 流程历史
     * 
     * @param id  流程历史ID
     * @return  流程历史
     */
    @Override
    public ActGrcInstanceHistoric selectActGrcInstanceHistoricById(Long id)
    {
        return actGrcInstanceHistoricMapper.selectActGrcInstanceHistoricById(id);
    }

    /**
     * 查询 流程历史列表
     * 
     * @param actGrcInstanceHistoric  流程历史
     * @return  流程历史
     */
    @Override
    public List<ActGrcInstanceHistoric> selectActGrcInstanceHistoricList(ActGrcInstanceHistoric actGrcInstanceHistoric)
    {
        return actGrcInstanceHistoricMapper.selectActGrcInstanceHistoricList(actGrcInstanceHistoric);
    }

    /**
     * 新增 流程历史
     * 
     * @param actGrcInstanceHistoric  流程历史
     * @return 结果
     */
    @Override
    public int insertActGrcInstanceHistoric(ActGrcInstanceHistoric actGrcInstanceHistoric)
    {
        return actGrcInstanceHistoricMapper.insertActGrcInstanceHistoric(actGrcInstanceHistoric);
    }

    /**
     * 修改 流程历史
     * 
     * @param actGrcInstanceHistoric  流程历史
     * @return 结果
     */
    @Override
    public int updateActGrcInstanceHistoric(ActGrcInstanceHistoric actGrcInstanceHistoric)
    {
        return actGrcInstanceHistoricMapper.updateActGrcInstanceHistoric(actGrcInstanceHistoric);
    }

    /**
     * 批量删除 流程历史
     * 
     * @param ids 需要删除的 流程历史ID
     * @return 结果
     */
    @Override
    public int deleteActGrcInstanceHistoricByIds(Long[] ids)
    {
        return actGrcInstanceHistoricMapper.deleteActGrcInstanceHistoricByIds(ids);
    }

    /**
     * 删除 流程历史信息
     * 
     * @param id  流程历史ID
     * @return 结果
     */
    @Override
    public int deleteActGrcInstanceHistoricById(Long id)
    {
        return actGrcInstanceHistoricMapper.deleteActGrcInstanceHistoricById(id);
    }
}
