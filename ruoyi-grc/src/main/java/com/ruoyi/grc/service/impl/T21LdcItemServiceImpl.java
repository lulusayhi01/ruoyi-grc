package com.ruoyi.grc.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.grc.utils.SeqUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.grc.mapper.T21LdcItemMapper;
import com.ruoyi.grc.domain.T21LdcItem;
import com.ruoyi.grc.service.IT21LdcItemService;

/**
 * 损失明细Service业务层处理
 * 
 * @author liuqi
 * @date 2021-01-14
 */
@Service
public class T21LdcItemServiceImpl implements IT21LdcItemService 
{
    @Autowired
    private T21LdcItemMapper t21LdcItemMapper;

    /**
     * 查询损失明细
     * 
     * @param id 损失明细ID
     * @return 损失明细
     */
    @Override
    public T21LdcItem selectT21LdcItemById(Long id)
    {
        return t21LdcItemMapper.selectT21LdcItemById(id);
    }

    /**
     * 查询损失明细列表
     * 
     * @param t21LdcItem 损失明细
     * @return 损失明细
     */
    @Override
    public List<T21LdcItem> selectT21LdcItemList(T21LdcItem t21LdcItem)
    {
        return t21LdcItemMapper.selectT21LdcItemList(t21LdcItem);
    }

    /**
     * 新增损失明细
     * 
     * @param t21LdcItem 损失明细
     * @return 结果
     */
    @Override
    public int insertT21LdcItem(T21LdcItem t21LdcItem)
    {
        t21LdcItem.setCreateTime(DateUtils.getNowDate());
        t21LdcItem.setCode(SeqUtils.getNextCode("T001"));
        return t21LdcItemMapper.insertT21LdcItem(t21LdcItem);
    }

    /**
     * 修改损失明细
     * 
     * @param t21LdcItem 损失明细
     * @return 结果
     */
    @Override
    public int updateT21LdcItem(T21LdcItem t21LdcItem)
    {
        t21LdcItem.setUpdateTime(DateUtils.getNowDate());
        return t21LdcItemMapper.updateT21LdcItem(t21LdcItem);
    }

    /**
     * 批量删除损失明细
     * 
     * @param ids 需要删除的损失明细ID
     * @return 结果
     */
    @Override
    public int deleteT21LdcItemByIds(Long[] ids)
    {
        return t21LdcItemMapper.deleteT21LdcItemByIds(ids);
    }

    /**
     * 删除损失明细信息
     * 
     * @param id 损失明细ID
     * @return 结果
     */
    @Override
    public int deleteT21LdcItemById(Long id)
    {
        return t21LdcItemMapper.deleteT21LdcItemById(id);
    }
}
