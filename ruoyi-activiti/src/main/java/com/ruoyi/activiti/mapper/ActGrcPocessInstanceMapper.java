package com.ruoyi.activiti.mapper;

import java.util.List;
import com.ruoyi.activiti.domain.ActGrcPocessInstance;

/**
 * 流程实例Mapper接口
 * 
 * @author ruoyi
 * @date 2020-12-06
 */
public interface ActGrcPocessInstanceMapper 
{
    /**
     * 查询流程实例
     * 
     * @param id 流程实例ID
     * @return 流程实例
     */
    public ActGrcPocessInstance selectActGrcPocessInstanceById(Long id);

    /**
     * 查询流程实例列表
     * 
     * @param actGrcPocessInstance 流程实例
     * @return 流程实例集合
     */
    public List<ActGrcPocessInstance> selectActGrcPocessInstanceList(ActGrcPocessInstance actGrcPocessInstance);

    /**
     * 新增流程实例
     * 
     * @param actGrcPocessInstance 流程实例
     * @return 结果
     */
    public int insertActGrcPocessInstance(ActGrcPocessInstance actGrcPocessInstance);

    /**
     * 修改流程实例
     * 
     * @param actGrcPocessInstance 流程实例
     * @return 结果
     */
    public int updateActGrcPocessInstance(ActGrcPocessInstance actGrcPocessInstance);

    /**
     * 删除流程实例
     * 
     * @param id 流程实例ID
     * @return 结果
     */
    public int deleteActGrcPocessInstanceById(Long id);

    /**
     * 批量删除流程实例
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteActGrcPocessInstanceByIds(Long[] ids);
}
