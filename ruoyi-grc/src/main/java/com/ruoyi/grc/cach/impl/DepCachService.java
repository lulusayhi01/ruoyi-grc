package com.ruoyi.grc.cach.impl;

import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.grc.cach.ICachService;
import com.ruoyi.system.mapper.SysDeptMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("depCachService")
public class DepCachService implements ICachService<SysDept> {

    @Autowired
    private SysDeptMapper deptMapper;
    @Override
    public List<SysDept> getCachDBList() {
        List<SysDept> sysDepts = deptMapper.allDeptList();
        for (SysDept sysDept : sysDepts) {
            Long[] longs = Convert.toLongArray("-", sysDept.getAllId());
            sysDept.setIdes(longs);
            sysDept.setNamees(sysDept.getAllName().split("-"));
        }
        return sysDepts;
    }
}
