package com.ruoyi;

import com.googlecode.aviator.AviatorEvaluator;
import com.googlecode.aviator.Expression;
import org.activiti.bpmn.model.Process;
import org.activiti.bpmn.model.*;
import org.activiti.engine.HistoryService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.image.ProcessDiagramGenerator;
import org.activiti.image.impl.DefaultProcessDiagramGenerator;

import java.io.*;
import java.util.*;

public class ActivitiFormateUtils {

    public static  StringBuffer formateLabelLocationMap(Map<String, GraphicInfo> labelLocationMap ,   StringBuffer stringBuffer ){

        if(stringBuffer ==null){
            stringBuffer=new StringBuffer();
        }
        final StringBuffer sb =new StringBuffer();
        labelLocationMap.forEach((s, graphicInfo) -> {

            double x = graphicInfo.getX();
            double y = graphicInfo.getY();
            sb.append(String.format("节点 :{%s}  x :{%s}  y: {%s}   "
                    ,s
                    ,x
                    ,y
                    ));
            sb.append("\r\n");

        });

        return  stringBuffer.append(sb);


    }


    public static  StringBuffer formateHistoricActivityInstance(HistoricActivityInstance hActivityInstance , StringBuffer stringBuffer ){
        if(stringBuffer ==null){
            stringBuffer=new StringBuffer();
        }
        return stringBuffer.append(String.format("hActivityInstance   Id :{%s}  TaskId :{%s}  Assignee: {%s} ActivityId ：{%s}  ActivityName：{%s} "
                ,hActivityInstance.getId()
                ,hActivityInstance.getTaskId()
                ,hActivityInstance.getAssignee()
                ,hActivityInstance.getActivityId()
                ,hActivityInstance.getActivityName()


        ));

    }
    public static  StringBuffer formateTask(Task task , StringBuffer stringBuffer ){
        if(stringBuffer ==null){
            stringBuffer=new StringBuffer();
        }
        stringBuffer.append(String.format("流程定义的id = %s "
                ,task.getProcessDefinitionId()

        ));
        stringBuffer.append("\r\n");
        stringBuffer.append((String.format("任务id = %s  任务名称 = %s   节点编号 = %s   实例的id = %s  执行id = %s  任务负责人= %s  Vari= %s   "
                ,task.getId()

                ,task.getName()
                ,task.getTaskDefinitionKey()
                ,task.getProcessInstanceId()
                ,task.getExecutionId()
                ,task.getAssignee()
                ,task.getProcessVariables()


        )));
        stringBuffer.append("\r\n");
        return stringBuffer;
    }

    public static  StringBuffer formateProcessInstance(ProcessInstance processInstance , StringBuffer stringBuffer ){
        if(stringBuffer ==null){
            stringBuffer=new StringBuffer();
        }
        stringBuffer.append(String.format("流程定义的id =  %s 部署的id =  %s "
                ,processInstance.getProcessDefinitionId()
                ,processInstance.getDeploymentId()
        ));
        stringBuffer.append("\r\n");
        stringBuffer.append((String.format(" 实例的id =   %s  流程实例的 id =  %s 当前活动的id = %s "
                ,processInstance.getProcessInstanceId()
                ,processInstance.getId()
                ,processInstance.getActivityId()

        )));
        stringBuffer.append("\r\n");
        return stringBuffer;
    }

    public static  StringBuffer formateDeployment(Deployment deployment , StringBuffer stringBuffer ){
        if(stringBuffer ==null){
            stringBuffer=new StringBuffer();
        }
        stringBuffer.append(String.format("流程部署id = %s 流程部署名称 = %s "
                ,deployment.getId()
                , deployment.getName()

        ));
        stringBuffer.append("\r\n");

        return stringBuffer;
    }

    public static  StringBuffer formateProcessDefinition(ProcessDefinition processDefinition , StringBuffer stringBuffer ){
        if(stringBuffer ==null){
            stringBuffer=new StringBuffer();
        }

        stringBuffer.append(String.format("流程定义 id :{%s}  DeploymentId:{%s}  name:{%s}   key:{%s}   version: {%s} "
                ,processDefinition.getId()
                , processDefinition.getName()
                ,processDefinition.getKey()
                ,processDefinition.getVersion()
                ,processDefinition.getDeploymentId()
        ));
        stringBuffer.append("\r\n");

        return stringBuffer;
    }


    public static  StringBuffer formatEndEvent( EndEvent  gateway , StringBuffer stringBuffer ){
        if(stringBuffer ==null){
            stringBuffer=new StringBuffer();
        }
        return stringBuffer.append(
                String.format( "BaseElement= id :{%s}  "
                        ,gateway.getId()
                ) +
                        String.format( "name :{%s}  "
                                ,gateway.getName()

                        ) +
                        String.format( "FlowNode=  InFlows :{%s} OutFlows :{%s} "
                                ,gateway.getIncomingFlows()
                                ,gateway.getOutgoingFlows()

                        ) +"\r\n"

        );
    }

    public static  StringBuffer formateGateway( Gateway  gateway , StringBuffer stringBuffer ){
        if(stringBuffer ==null){
            stringBuffer=new StringBuffer();
        }
       return stringBuffer.append(
                        String.format( "BaseElement= id :{%s}  "
                                ,gateway.getId()
                        ) +
                        String.format( "name :{%s}  "
                                ,gateway.getName()

                        ) +
                        String.format( "FlowNode=  InFlows :{%s} OutFlows :{%s} "
                                ,gateway.getIncomingFlows()
                                ,gateway.getOutgoingFlows()

                        ) +
                        String.format( "gateway= defaultFlow :{%s} "
                                ,gateway.getDefaultFlow()
                        )+"\r\n"
        );
    }

    public static  StringBuffer formateUserTask( UserTask  userTask , StringBuffer stringBuffer ){
        if(stringBuffer ==null){
            stringBuffer=new StringBuffer();
        }

       return stringBuffer.append(
                        String.format( "BaseElement= id :{%s}  "
                                ,userTask.getId()
                        ) +
                        String.format( "name :{%s}  "
                                ,userTask.getName()

                        ) +
                        String.format( "FlowNode=  InFlows :{%s} OutFlows :{%s} "
                                ,userTask.getIncomingFlows()
                                ,userTask.getOutgoingFlows()

                        ) +
                        String.format( "userTask= Assignee :{%s}  category : {%s}   skipExpression ：{%s}    "
                                ,userTask.getAssignee()
                                ,userTask.getCategory()
                                ,userTask.getSkipExpression()
                                //,userTask.getCustomUserIdentityLinks()
                                //,userTask.getCustomGroupIdentityLinks()
                                // ,userTask.getCustomProperties()
                                //customUserIdentityLinks  ：{%s}
                                //customGroupIdentityLinks  ：{%s}
                                //customProperties  ：{%s}
                                // ,userTask.getCandidateUsers()
                                // ,userTask.getCandidateGroups()
                        )+"\r\n"

        );
    }

    public static  StringBuffer formateSequenceFlow( SequenceFlow  sequenceFlow , StringBuffer stringBuffer ){
        if(stringBuffer ==null){
            stringBuffer=new StringBuffer();
        }

        return stringBuffer.append(
                                String.format( "BaseElement= id :{%s}  "
                                        ,sequenceFlow.getId()

                                ) +
                                String.format( " Name : {%s}  "
                                        ,sequenceFlow.getName()

                                ) +
                                String.format( "SequenceFlow= ref :{%s}    target: {%s}   Condition:  {%s}  "
                                        ,sequenceFlow.getSourceRef()
                                        ,sequenceFlow.getTargetRef()
                                        ,sequenceFlow.getConditionExpression()
                                        //,sequenceFlow.getSourceFlowElement()
                                        // ,sequenceFlow.getTargetFlowElement()
                                        //,sequenceFlow.getExecutionListeners()
                                        // ,sequenceFlow.getExtensionElements().keySet()

                                )+"\r\n"




        );
    }




}
