package com.ruoyi.activiti.service.impl;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import com.github.pagehelper.Page;
import com.googlecode.aviator.AviatorEvaluator;
import com.googlecode.aviator.Expression;
import com.ruoyi.activiti.domain.ActGrcPocessVariables;
import com.ruoyi.activiti.utils.ActivitiUtils;
import com.ruoyi.activiti.utils.GrcPocess;
import com.ruoyi.activiti.utils.GrcPocessInst;
import com.ruoyi.activiti.utils.GrcUserTask;
import com.ruoyi.activiti.utils.graph.DataGraph;
import com.ruoyi.activiti.utils.graph.LinksGraph;
import com.ruoyi.activiti.utils.graph.SeriesGraph;
import com.ruoyi.common.core.page.PageDomain;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import org.activiti.bpmn.model.*;
import org.activiti.engine.*;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricVariableInstance;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.activiti.mapper.ActGrcPocessInstanceMapper;
import com.ruoyi.activiti.domain.ActGrcPocessInstance;
import com.ruoyi.activiti.service.IActGrcPocessInstanceService;

/**
 * 流程实例Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-12-06
 */
@Service
public class ActGrcPocessInstanceServiceImpl implements IActGrcPocessInstanceService
{
    @Autowired
    private ActGrcPocessInstanceMapper actGrcPocessInstanceMapper;

    @Autowired
    private ProcessEngine processEngine ;
    @Autowired
    private RepositoryService repositoryService ;
    @Autowired
    private RuntimeService runtimeService ;

    @Autowired
    private TaskService taskService ;
    @Autowired
    private HistoryService historyService ;
    @Autowired
    private ManagementService managementService ;

    @Override
    public List<GrcUserTask> startGetNextGrcUserTask(String definitionKey) {

        ProcessDefinitionQuery processDefinitionQuery = repositoryService.createProcessDefinitionQuery();
        //设置条件，并查询出当前的所有流程定义
        List<ProcessDefinition> processDefinitionList = processDefinitionQuery.processDefinitionKey(definitionKey)
                .orderByProcessDefinitionVersion()
                .desc()
                .list();

        GrcPocess grcPocess = ActivitiUtils.pocessALL(repositoryService, processDefinitionList.get(0).getId());
        GrcUserTask grcUserTask = grcPocess.startNextGrcUserTask();
        ArrayList<GrcUserTask> grcUserTasks = new ArrayList<>();
        grcUserTasks.add(grcUserTask);
        return grcUserTasks;
    }

    @Override
    public ActGrcPocessVariables getProcessInstance(String instanceId) {
        ActGrcPocessVariables actGrcPocessVariables = new ActGrcPocessVariables();

        ProcessInstance pi = runtimeService.createProcessInstanceQuery()
                .processInstanceId(instanceId)
                .singleResult();//返回的数据要么是单行，要么是空 ，其他情况报错

        List<Task> taskList = taskService.createTaskQuery()
                //.taskId("ad5db454-44e9-11eb-942f-b0359fadfbca")
                //.executionId("160001")
                .processInstanceId(instanceId)
                // .processDefinitionKey(definitionKey)
                //.processDefinitionId("lossevent_6:4:120003")
                // .taskCandidateUser("")
                //.taskAssignee("李四")
                .list();
        if(taskList.size()>0){
            Task task=taskList.get(0);


            //变量
            Map<String, Object> variables = taskService.getVariables(task.getId());
            actGrcPocessVariables.setVars(variables);

            actGrcPocessVariables.setProInstanceId(instanceId);

            actGrcPocessVariables.setTargetId(task.getId());


            List<HistoricActivityInstance> unFinishedlist = historyService.createHistoricActivityInstanceQuery()
                    .processInstanceId(instanceId)
                    .unfinished()
                    .list();
            HistoricActivityInstance historicActivityInstance = unFinishedlist.get(0);
            actGrcPocessVariables.setUser(historicActivityInstance.getAssignee());
            actGrcPocessVariables.setActivityId(historicActivityInstance.getActivityId());
            actGrcPocessVariables.setActivityName(historicActivityInstance.getActivityName());

            actGrcPocessVariables.setBefinitionKey(pi.getBusinessKey());

        }
        return actGrcPocessVariables;
    }

    @Override
    public ActGrcPocessInstance startAndCompleteProcessInstance(ActGrcPocessVariables actGrcPocessVariables) {
        //启动参数
        Map<String, Object> variables = getStringObjectMap(actGrcPocessVariables);
        variables.put("user", SecurityUtils.getLoginUser().getUsername()); //当前用户=拟稿人
        // variables.put("acti_go",true); //默认

        //启动
        ProcessInstance  processInstance = checkStartProcessInstance(actGrcPocessVariables, variables);



        ActGrcPocessInstance actGrcPocessInstance = new ActGrcPocessInstance();
        actGrcPocessInstance.setProInstanceId(processInstance.getProcessInstanceId());
        //提交下一步
        List<Task> list = taskService.createTaskQuery().processInstanceId(processInstance.getProcessInstanceId()).list();
        //表达式定义下一个人
        variables.put("user",actGrcPocessVariables.getUser());


        //api定义下一个人
        //taskService.setAssignee(list.get(0).getId(),actGrcPocessVariables.getUser());
        taskService.complete(list.get(0).getId(),variables);


        return actGrcPocessInstance;
    }

    @Override
    public ActGrcPocessInstance completeProcessInstance( ActGrcPocessVariables actGrcPocessVariables) {


        //参数
        Map<String, Object> variables = getStringObjectMap(actGrcPocessVariables);
        variables.put("user",actGrcPocessVariables.getUser());
        //api定义下一个人
        //taskService.setAssignee(actGrcPocessVariables.getTaskId(),actGrcPocessVariables.getUser());

        //变量设置
        taskService.setVariables(actGrcPocessVariables.getTaskId(),variables);
        //提交
        taskService.complete(actGrcPocessVariables.getTaskId());

        return null;
    }
   /* //设置下一步的审批人
    public void  setAssignee(String processInstanceId,String userName){

        List<Task> list = taskService.createTaskQuery().processInstanceId(processInstanceId).list();
        if(list.size()>0){
            taskService.setAssignee(list.get(0).getId(),userName);
        }

    }*/
    //
    public String  getProcessInstanceIdByTaskId(String taskId){

        List<Task> list = taskService.createTaskQuery()
                .taskId(taskId)
                .list();
        if(list.size()>0){
            return  list.get(0).getProcessInstanceId();
        }
        return  "";

    }


    private ProcessInstance checkStartProcessInstance(ActGrcPocessVariables actGrcPocessVariables, Map<String, Object> variables) {
        ProcessInstance processInstance;
       /* List<ProcessInstance> instancelist = runtimeService.createProcessInstanceQuery()
                //.processInstanceId("eb18dc4a-468d-11eb-b3a5-b0359fadfbca")
                .processInstanceBusinessKey(actGrcPocessVariables.getBusinessId())
                .processDefinitionKey(actGrcPocessVariables.getBefinitionKey())
                .list();
        if(instancelist.size()>0){//存在任务
            processInstance=instancelist.get(0);
        }else{ //正常启动
                processInstance = runtimeService.startProcessInstanceByKey(
                actGrcPocessVariables.getBefinitionKey(),
                actGrcPocessVariables.getBusinessId(),
                 variables);
        }*/


        //得到ProcessDefinitionQuery对象
        ProcessDefinitionQuery processDefinitionQuery = repositoryService.createProcessDefinitionQuery();
        //设置条件，并查询出当前的所有流程定义
        List<ProcessDefinition> processDefinitionList = processDefinitionQuery.processDefinitionKey(actGrcPocessVariables.getBefinitionKey())
                .orderByProcessDefinitionVersion()
                .desc()
                .list();

        variables.put("definition_key",processDefinitionList.get(0).getKey());
        variables.put("definition_name",processDefinitionList.get(0).getName());
        variables.put("businessId",actGrcPocessVariables.getBusinessId());

        String processDefinitionId = actGrcPocessVariables.getProcessDefinitionId();
        if(StringUtils.isNotEmpty(processDefinitionId)){ // id启用
            processInstance =  runtimeService.startProcessInstanceById(processDefinitionId, actGrcPocessVariables.getBusinessId(),variables);
        }else{//key
            processInstance = runtimeService.startProcessInstanceByKey(
                    actGrcPocessVariables.getBefinitionKey(),
                    actGrcPocessVariables.getBusinessId(),
                    variables);
        }


        return processInstance;
    }


    private Map<String, Object> getStringObjectMap(ActGrcPocessVariables actGrcPocessVariables) {
        Map<String, Object> variables = new HashMap<>();
        if(actGrcPocessVariables.getVars()!=null){
            variables.putAll(actGrcPocessVariables.getVars());
        }
        return variables;
    }
    @Override
    public ActGrcPocessInstance skipProcessInstance( ActGrcPocessVariables actGrcPocessVariables) {
        List<Task> list = taskService.createTaskQuery().taskId(actGrcPocessVariables.getTaskId()).list();
        Task task = list.get(0);
        GrcPocess grcPocess = ActivitiUtils.pocessALL(processEngine.getRepositoryService(), task.getProcessDefinitionId());

        FlowNode flowNode = ActivitiUtils.activFlowNode(processEngine, task);

        //1 记录原活动方向
        List<SequenceFlow> oriSequenceFlows = new ArrayList<SequenceFlow>();
        oriSequenceFlows.addAll(flowNode.getOutgoingFlows());

        //2 清理活动方向
        flowNode.getOutgoingFlows().clear();

        //3 建立新方向
        List<SequenceFlow> newSequenceFlowList = new ArrayList<SequenceFlow>();
        SequenceFlow newSequenceFlow = new SequenceFlow();
        newSequenceFlow.setId("newSequenceFlowId");
        newSequenceFlow.setSourceFlowElement(flowNode);
        newSequenceFlow.setTargetFlowElement(grcPocess.getUserTaskByActivityId( actGrcPocessVariables .getTargetId())); //目标id
        newSequenceFlowList.add(newSequenceFlow);
        flowNode.setOutgoingFlows(newSequenceFlowList);
        //4 提交
        completeProcessInstance(actGrcPocessVariables);

        //5 恢复
        flowNode.setOutgoingFlows(oriSequenceFlows);

        return null;
    }

    @Override
    public List<ActGrcPocessVariables> buildActGrcPocessVariables(List<GrcUserTask> grcUserTasks) {
        return grcUserTasks.stream().map(ActGrcPocessVariables::new).collect(Collectors.toList());
    }

    @Override
    public List<ActGrcPocessVariables> buildActGrcInstanceHistoric(List<HistoricActivityInstance> historicActivityInstanceList) {
        return historicActivityInstanceList.stream().map(ActGrcPocessVariables::new).collect(Collectors.toList());
    }


    @Override
    public List<GrcUserTask> NextUserTask(String taskId) {

        // 重置
        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("acti_go",true);
        variables.put("acti_back",true);
        taskService.setVariables(taskId,variables);

        List<Task> list = taskService.createTaskQuery().taskId(taskId).list();
        Task task = list.get(0);
        GrcPocess grcPocess = ActivitiUtils.pocessALL(processEngine.getRepositoryService(), task.getProcessDefinitionId());

        //获取流程实例 历史节点（需要完成）
        List<HistoricActivityInstance> unFinishedlist = historyService.createHistoricActivityInstanceQuery()
                .processInstanceId(task.getProcessInstanceId())
                .unfinished()
                .list();
        HistoricActivityInstance hActivityInstance=unFinishedlist.get(0);
        List<GrcUserTask> nextUserTask = grcPocess.getNextUserTask(hActivityInstance.getActivityId());

        LinkedList<GrcUserTask> nextTrueUserTask = new LinkedList<>();
        Map<String, Object> env = taskService.getVariables(task.getId());

        for (GrcUserTask grcUserTask : nextUserTask) {
            if(grcUserTask.getUserTask()!=null){
                String expression = grcUserTask.getConditionExpression();
                Expression compiledExp = AviatorEvaluator.compile(expression);
                Boolean result = (Boolean) compiledExp.execute(env);
                //   boolean isFuture = grcUserTask.getIsFuture(env);
                //System.out.println("是否成立："+result);
                //System.out.println(grcUserTask);
                if(result==null|| result){
                    nextTrueUserTask.add(grcUserTask);
                }
            }else if(grcUserTask.getEndEvent() !=null){
                nextTrueUserTask.add(grcUserTask);
            }

        }
        return nextTrueUserTask;
    }


    @Override
    public  List<GrcUserTask> getPreGrcUserTask(String  taskId) {
        List<Task> list = taskService.createTaskQuery().taskId(taskId).list();
        Task task = list.get(0);
        GrcPocess grcPocess = getGrcPocess(task.getProcessDefinitionId());

        GrcPocessInst grcPocessInst = new GrcPocessInst();
        grcPocessInst.setGrcPocess(grcPocess);

        //获取流程实例 历史节点（已完成）
        List<HistoricActivityInstance> finishedlist = historyService.createHistoricActivityInstanceQuery()
                .processInstanceId(task.getProcessInstanceId())
                .finished()
                .list();
        grcPocessInst.setALLfinishedlist(finishedlist);

        //获取流程实例 历史节点（需要完成）
        List<HistoricActivityInstance> unFinishedlist = historyService.createHistoricActivityInstanceQuery()
                .processInstanceId(task.getProcessInstanceId())
                .unfinished()
                .list();
        grcPocessInst.setThisActivity(unFinishedlist.get(0));
        List<GrcUserTask> preGrcUserTaskList = grcPocessInst.getPreGrcUserTaskList();
        return preGrcUserTaskList;
    }

    @Override
    public Map<String, Object> getHistoricVariable(String taskId) {
        List<HistoricVariableInstance> list = historyService.createHistoricVariableInstanceQuery().taskId(taskId).list();
        HashMap<String, Object> stringObjectHashMap = new HashMap<>();
        for (HistoricVariableInstance historicVariableInstance : list) {
            stringObjectHashMap.put(historicVariableInstance.getVariableName(),historicVariableInstance.getValue());
        }
        return stringObjectHashMap;

    }

    @Override
    public GrcPocess getGrcPocess(String processDefinitionId) {
        GrcPocess grcPocess = ActivitiUtils.pocessALL(processEngine.getRepositoryService(), processDefinitionId);
        return grcPocess;
    }

    @Override
    public SeriesGraph getSeriesGraph(String processDefinitionId) {

        SeriesGraph seriesGraph = new SeriesGraph();

        GrcPocess grcPocess = ActivitiUtils.pocessALL(processEngine.getRepositoryService(), processDefinitionId);
        BpmnModel bpmnModel = grcPocess.getBpmnModel();

        //所有的元素
        Map<String, GraphicInfo> labelLocationMap = bpmnModel.getLocationMap();
        //下标
        AtomicInteger idex = new AtomicInteger();
        labelLocationMap.forEach((s, graphicInfo) -> {

                     FlowElement flowElement = grcPocess.getElementById(s);
                     double x = graphicInfo.getX();
                     double y = graphicInfo.getY();
                     String name = flowElement.getName();
                     String elementId = flowElement.getId();
                     DataGraph dataGraph = new DataGraph(elementId,name, x, y);

                        dataGraph.setIndex(idex.getAndIncrement());

                        if (flowElement instanceof Gateway ) {
                            dataGraph.setSymbol("diamond");
                            dataGraph.setSymbolSize( new int[]{50,50});
                        } else if (flowElement instanceof StartEvent
                                    || flowElement instanceof EndEvent) {
                            dataGraph.setSymbol("circle");
                            dataGraph.setSymbolSize( new int[]{50,50});
                        }


                     seriesGraph.getData().add(dataGraph);

                }
         );
        //所有的线
        idex.set(0);
        Map<String, List<GraphicInfo>> flowLocationMap = bpmnModel.getFlowLocationMap();

        for (Map.Entry<String, List<GraphicInfo>> stringListEntry : flowLocationMap.entrySet()) {
            String s = stringListEntry.getKey();
            SequenceFlow sequenceFlow =(SequenceFlow) grcPocess.getElementById(s);
            String sourceRef = sequenceFlow.getSourceRef();
            String targetRef = sequenceFlow.getTargetRef();
            int source = seriesGraph.getDataIdex(sourceRef);
            int target = seriesGraph.getDataIdex(targetRef);
            LinksGraph linksGraph = new LinksGraph(source, target);
            if("${acti_back}".equals(sequenceFlow.getConditionExpression())){ //退回线条 弧度处理
                linksGraph.appendLineStyle("curveness",0.8);
            }
            String name = sequenceFlow.getName();
            if(StringUtils.isNotEmpty(name)){  //线条的名称
                linksGraph.getValue().add(name);
            }
            seriesGraph.getLinks().add(linksGraph);

        }


        return seriesGraph;
    }


    @Override
    public List<HistoricActivityInstance> getALLPreGrcUserTask(String  proInstanceId) {
        List<HistoricActivityInstance> finishedlist = historyService.createHistoricActivityInstanceQuery()
                .processInstanceId(proInstanceId)
                .orderByHistoricActivityInstanceStartTime().asc()

                .list();

        return finishedlist;
    }

    @Override
    public SeriesGraph getSeriesGraphBYproInstanceId(String processInstanceId) {
        // 第一步 历史记录
        List<HistoricActivityInstance> allPreGrcUserTask = getALLPreGrcUserTask(processInstanceId);
        List<ActGrcPocessVariables> actGrcPocessVariables = buildActGrcInstanceHistoric(allPreGrcUserTask);
        //变量处理
        for (ActGrcPocessVariables actGrcPocessVariable : actGrcPocessVariables) {
            String taskId = actGrcPocessVariable.getTaskId();
            Date updateTime = actGrcPocessVariable.getUpdateTime(); //结束时间为 null 则 没有审批
            if(StringUtils.isNotEmpty(taskId) && updateTime != null){
                Map<String, Object> historicVariable = getHistoricVariable(taskId);
                actGrcPocessVariable.setVars(historicVariable);
            }

        }
        //  第二步  获取完整地图
        String processDefinitionId = allPreGrcUserTask.get(0).getProcessDefinitionId();
        SeriesGraph seriesGraph = getSeriesGraph(processDefinitionId);


        // 第三步 结合
        List<ActGrcPocessVariables> collect = actGrcPocessVariables.stream().
                filter(actGrcPocessVariables1 -> StringUtils.isNotEmpty(actGrcPocessVariables1.getUser())).collect(Collectors.toList());
       /* if(collect.size()>0){
            //删除最后一个 代办
            collect.remove(collect.size()-1);
        }*/
        seriesGraph.setActGrcPocessVariables(collect);
        /*HashMap<String, String> variableMap = new HashMap<>();
        variableMap.put("remark","备注");*/
        // 第三步 上色
        String preActivityId="";
        int predataIdex= 0;
        for (ActGrcPocessVariables actGrcPocessVariable : actGrcPocessVariables) {
            String activityId = actGrcPocessVariable.getActivityId();
            String activityName = actGrcPocessVariable.getActivityName();
            String user = actGrcPocessVariable.getUser();

            Map<String, Object> vars = actGrcPocessVariable.getVars();
            Date createTime = actGrcPocessVariable.getCreateTime();
            if(StringUtils.isNotEmpty(activityId)){

                DataGraph dataGraph = seriesGraph.getDataGraph(activityId);
                int dataIdex = seriesGraph.getDataIdex(activityId);

                 //日志处理
                if( StringUtils.isNotEmpty(activityName)&& StringUtils.isNotEmpty(user)
                        && actGrcPocessVariable.getUpdateTime()!=null  // UpdateTime 是结束时间  如果没有则是 未完成的任务
                ){
                    //节点的描述信息
                    String remark = String.format(" 用户:{%s}    时间: {%s}   备注:  {%s}  "
                            , user
                            , DateUtils.parseDateToStr(DateUtils.YYYY_MM_DD_HH_MM_SS,createTime)
                            , vars.get("remark")
                    );
                    dataGraph.getValue().add(remark);
                }

                //高亮处理
                if(StringUtils.isNotEmpty(preActivityId) && !preActivityId.equals(activityId) ){
                    //元素
                    dataGraph.appenditemStyle("color","#F56C6C");
                    //线条
                    LinksGraph linksGraph = seriesGraph.getLinksGraph(predataIdex, dataIdex);
                    if(linksGraph!=null){
                        linksGraph.appendLineStyle("color","#F56C6C");
                    }

                }else if(StringUtils.isEmpty(preActivityId)){
                    //第一为红色
                    dataGraph.appenditemStyle("color","#F56C6C");
                }
                preActivityId=activityId;
                predataIdex=dataIdex;
            }
        }
        return seriesGraph;
    }

    @Override
    public List<HistoricProcessInstance> getHistoricProcessInstance(String businessId, String befinitionKey) {

        List<HistoricProcessInstance> list = historyService.createHistoricProcessInstanceQuery()
                .processInstanceBusinessKey(businessId)
                .processDefinitionKey(befinitionKey)
                .orderByProcessInstanceStartTime().desc()
                .list();
        return  list;
    }

    @Override
    public List<String> getProcessInstanceIdList(String processDefinitionKey, String businessKey) {
        List<HistoricProcessInstance> list = historyService.createHistoricProcessInstanceQuery().processDefinitionKey(processDefinitionKey).processInstanceBusinessKey("1").list();
        List<String> collect = list.stream().map(historicProcessInstance -> historicProcessInstance.getId()).collect(Collectors.toList());
        return collect;
    }

    @Override
    public Page<ActGrcPocessVariables> getTaskList(ActGrcPocessVariables actGrcPocessVariables, PageDomain pageDomain) {

        TaskQuery taskQuery = taskService.createTaskQuery().taskAssignee(SecurityUtils.getLoginUser().getUsername());
        if(StringUtils.isNotEmpty(actGrcPocessVariables.getBefinitionKey())){
            taskQuery=taskQuery.processDefinitionKey(actGrcPocessVariables.getBefinitionKey());
        }
        taskQuery=taskQuery.orderByTaskCreateTime().desc();
        long count = taskQuery.count();
        List<Task> taskList =taskQuery.listPage((pageDomain.getPageNum() - 1) * pageDomain.getPageSize(),pageDomain.getPageSize());
               // .listPage((pageDomain.getPageNum() - 1) * pageDomain.getPageSize(),pageDomain.getPageSize());
              //  .processDefinitionKey("")
              // .list();

        LinkedList<ActGrcPocessVariables> actGrcPocessVariablesCompletes = new LinkedList<>();
        for (Task task : taskList) {
            ActGrcPocessVariables actGrcPocess= new ActGrcPocessVariables();
            Map<String, Object> variables = taskService.getVariables(task.getId());
            actGrcPocess.setTaskId(task.getId());
            actGrcPocess.setVars(variables);
            actGrcPocess.setCreateTime(task.getCreateTime());

            actGrcPocess.setProInstanceId(task.getProcessInstanceId());

            String taskDefinitionKey = task.getTaskDefinitionKey();
            String name = task.getName();



            actGrcPocess.setActivityId(taskDefinitionKey);
            actGrcPocess.setActivityName(name);

            actGrcPocessVariablesCompletes.add(actGrcPocess);
        }
        Page<ActGrcPocessVariables> list = new Page<ActGrcPocessVariables>();
        list.setTotal(count);
        list.addAll(actGrcPocessVariablesCompletes);
        return list;
    }
    //-----------------------------------------------------------------------------------------

    /**
     * 查询流程实例
     * 
     * @param id 流程实例ID
     * @return 流程实例
     */
    @Override
    public ActGrcPocessInstance selectActGrcPocessInstanceById(Long id)
    {
        return actGrcPocessInstanceMapper.selectActGrcPocessInstanceById(id);
    }

    /**
     * 查询流程实例列表
     * 
     * @param actGrcPocessInstance 流程实例
     * @return 流程实例
     */
    @Override
    public List<ActGrcPocessInstance> selectActGrcPocessInstanceList(ActGrcPocessInstance actGrcPocessInstance)
    {
        return actGrcPocessInstanceMapper.selectActGrcPocessInstanceList(actGrcPocessInstance);
    }

    /**
     * 新增流程实例
     * 
     * @param actGrcPocessInstance 流程实例
     * @return 结果
     */
    @Override
    public int insertActGrcPocessInstance(ActGrcPocessInstance actGrcPocessInstance)
    {
        return actGrcPocessInstanceMapper.insertActGrcPocessInstance(actGrcPocessInstance);
    }

    /**
     * 修改流程实例
     * 
     * @param actGrcPocessInstance 流程实例
     * @return 结果
     */
    @Override
    public int updateActGrcPocessInstance(ActGrcPocessInstance actGrcPocessInstance)
    {
        return actGrcPocessInstanceMapper.updateActGrcPocessInstance(actGrcPocessInstance);
    }

    /**
     * 批量删除流程实例
     * 
     * @param ids 需要删除的流程实例ID
     * @return 结果
     */
    @Override
    public int deleteActGrcPocessInstanceByIds(Long[] ids)
    {
        return actGrcPocessInstanceMapper.deleteActGrcPocessInstanceByIds(ids);
    }

    /**
     * 删除流程实例信息
     * 
     * @param id 流程实例ID
     * @return 结果
     */
    @Override
    public int deleteActGrcPocessInstanceById(Long id)
    {
        return actGrcPocessInstanceMapper.deleteActGrcPocessInstanceById(id);
    }
}
