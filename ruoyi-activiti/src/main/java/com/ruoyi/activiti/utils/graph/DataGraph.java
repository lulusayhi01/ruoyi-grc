package com.ruoyi.activiti.utils.graph;

import com.ruoyi.common.utils.StringUtils;
import org.activiti.bpmn.model.SequenceFlow;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class DataGraph {
    private int index;
    private String name;
    private String elementid;
    private LinkedList<String>  value = new LinkedList<String>();
    private double x;
    private double y;


    private String symbol;
    private int[] symbolSize;


    private Map<String,Object> itemStyle ;


    public void appenditemStyle(String key,Object obj){
        if(this.itemStyle == null){
            this.itemStyle= new HashMap<String,Object>();
        }
        this.itemStyle.put(key,obj);
    }

    public DataGraph() {
    }

    public DataGraph(String elementid,String name, double x, double y) {
        this.elementid = elementid;
        this.name = name;
        this.x = x;
        this.y = y;
    }

    public DataGraph(String name, LinkedList<String> value, double x, double y, Map<String, Object> itemStyle) {
        this.name = name;
        this.value = value;
        this.x = x;
        this.y = y;
        this.itemStyle = itemStyle;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public int getIndex() {
        return index;
    }

    public String getElementid() {
        return elementid;
    }

    public void setElementid(String elementid) {
        this.elementid = elementid;
    }

    public void setIndex(int index) {
        if(StringUtils.isNotEmpty(this.name )){
            this.name =index+": "+ this.name;
        }else{
            this.name =index+"";
        }

        this.index = index;
    }

    public int[] getSymbolSize() {
        return symbolSize;
    }

    public void setSymbolSize(int[] symbolSize) {
        this.symbolSize = symbolSize;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LinkedList<String> getValue() {
        return value;
    }

    public void setValue(LinkedList<String> value) {
        this.value = value;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public Map<String, Object> getItemStyle() {
        return itemStyle;
    }

    public void setItemStyle(Map<String, Object> itemStyle) {
        this.itemStyle = itemStyle;
    }
}
