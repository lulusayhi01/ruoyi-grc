package com.ruoyi.activiti.domain;

import cn.hutool.core.annotation.AnnotationUtil;
import com.ruoyi.activiti.domain.dto.ActWorkflowFormDataDTO;
import com.ruoyi.common.core.domain.BaseEntity;
import org.activiti.api.task.model.Task;
import org.activiti.engine.impl.util.ReflectUtil;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.validation.constraints.NotBlank;
import java.lang.reflect.Field;

/**
 * 动态单对象 act_workflow_formdata
 * 
 * @author danny
 * @date 2020-11-02
 */
public class ActWorkflowFormDataParmiter extends ActWorkflowFormData
{
    public static void main(String[] args) throws Exception {
        ActWorkflowFormDataParmiter actWorkflowFormDataParmiter = new ActWorkflowFormDataParmiter();

        actWorkflowFormDataParmiter.setBusinessKey("11");
        System.out.println(actWorkflowFormDataParmiter.getBusinessKey());
        System.out.println(actWorkflowFormDataParmiter.getBusinessKeyP());



        Field controlId = ReflectUtil.getField("businessKey", ActWorkflowFormDataParmiter.class);
        Object value = AnnotationUtil.getAnnotationValue(controlId, NotBlank.class,"message");
        System.out.println(value);


    }
    /** 表单id */
    @NotBlank(message = "controlId不能为空")
    private String controlId;
    /** 表单名称 */
    @NotBlank(message = "controlId2不能为空")
    private String controlId2;
    @NotBlank(message = "controlName不能为空")
    private String controlName;
    @NotBlank(message = "controlName2不能为空")
    private String controlName2;


    private String businessKey;


    public String getBusinessKeyP() {
        return super.getBusinessKey();
    }

    @Override
    public String getBusinessKey() {
        return businessKey;
    }

    @Override
    public void setBusinessKey(String businessKey) {
        this.businessKey = businessKey;
    }

    @Override
    public String getControlId() {
        return controlId;
    }

    public String getControlId2() {
        return controlId2;
    }

    @Override
    public String getControlName() {
        return controlName;
    }

    public String getControlName2() {
        return controlName2;
    }

    @Override
    public void setControlId(String controlId) {
        this.controlId = controlId;
    }

    public void setControlId2(String controlId2) {
        this.controlId2 = controlId2;
    }

    @Override
    public void setControlName(String controlName) {
        this.controlName = controlName;
    }

    public void setControlName2(String controlName2) {
        this.controlName2 = controlName2;
    }
}
