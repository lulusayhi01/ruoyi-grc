package com.ruoyi.activiti.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.activiti.domain.ActGrcInstanceHistoric;
import com.ruoyi.activiti.domain.ActGrcPocess;
import com.ruoyi.activiti.domain.ActGrcPocessInstance;
import com.ruoyi.activiti.domain.ActGrcPocessVariables;
import com.ruoyi.activiti.utils.GrcPocess;
import com.ruoyi.activiti.utils.GrcUserTask;
import com.ruoyi.activiti.utils.graph.SeriesGraph;
import com.ruoyi.common.core.domain.TreeSelect;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.page.PageDomain;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.task.Task;
import com.github.pagehelper.Page;
/**
 * 流程实例Service接口
 * 
 * @author ruoyi
 * @date 2020-12-06
 */
public interface IActGrcPocessInstanceService 
{



    /**
     * 查看代办任务
     * @return
     */
    public Page<ActGrcPocessVariables>  getTaskList(ActGrcPocessVariables actGrcPocessVariables, PageDomain pageDomain);

    /**
     * 启动是获取拟稿人的下一个节点
     * @param definitionKey
     * @return
     */
    public List<GrcUserTask> startGetNextGrcUserTask(String definitionKey);


    /**
     * 获取流程执行情况
     * @param instanceId
     * @return
     */

    public ActGrcPocessVariables getProcessInstance(String instanceId);

    /**
     * 启动流程 并提交下一级
     *
     * @param actGrcPocessVariables
     * @return
     */
    public ActGrcPocessInstance startAndCompleteProcessInstance(ActGrcPocessVariables actGrcPocessVariables);

    /**
     * 提交流程

     * @param actGrcPocessVariables
     * @return
     */
    public ActGrcPocessInstance completeProcessInstance( ActGrcPocessVariables actGrcPocessVariables);

    /**
     * 跳跃历史节点
     * @param actGrcPocessVariables
     * @return
     */
    public ActGrcPocessInstance skipProcessInstance(ActGrcPocessVariables actGrcPocessVariables);


    public List<ActGrcPocessVariables> buildActGrcPocessVariables(List<GrcUserTask> grcUserTasks);


    public List<ActGrcPocessVariables> buildActGrcInstanceHistoric(List<HistoricActivityInstance> grcUserTasks);



    /**
     * 查看下一个节点
     * @param taskId
     * @return
     */
    public  List<GrcUserTask> NextUserTask(String  taskId);



    /**
     * 获取最新的历史节点
     * @param taskId  任务id
     */
    public  List<GrcUserTask> getPreGrcUserTask(String  taskId);

    public Map<String,Object>  getHistoricVariable(String  taskId);

    public GrcPocess getGrcPocess(String  processDefinitionId);


    public SeriesGraph getSeriesGraph(String  processDefinitionId);

    /**
     * 获取全部的历史节点
     * @param proInstanceId 流程实例ID
     * @return
     */
    public  List<HistoricActivityInstance> getALLPreGrcUserTask(String  proInstanceId);

    public  SeriesGraph getSeriesGraphBYproInstanceId(String  processInstanceId);

    public  List<HistoricProcessInstance>  getHistoricProcessInstance(String  businessId,String befinitionKey);




    /**
     * 获取
      * @param processDefinitionKey
     * @param businessKey
     * @return
     */
    public  List<String> getProcessInstanceIdList(String  processDefinitionKey, String businessKey);






    /**
     * 查询流程实例
     * 
     * @param id 流程实例ID
     * @return 流程实例
     */
    public ActGrcPocessInstance selectActGrcPocessInstanceById(Long id);

    /**
     * 查询流程实例列表
     * 
     * @param actGrcPocessInstance 流程实例
     * @return 流程实例集合
     */
    public List<ActGrcPocessInstance> selectActGrcPocessInstanceList(ActGrcPocessInstance actGrcPocessInstance);

    /**
     * 新增流程实例
     * 
     * @param actGrcPocessInstance 流程实例
     * @return 结果
     */
    public int insertActGrcPocessInstance(ActGrcPocessInstance actGrcPocessInstance);

    /**
     * 修改流程实例
     * 
     * @param actGrcPocessInstance 流程实例
     * @return 结果
     */
    public int updateActGrcPocessInstance(ActGrcPocessInstance actGrcPocessInstance);

    /**
     * 批量删除流程实例
     * 
     * @param ids 需要删除的流程实例ID
     * @return 结果
     */
    public int deleteActGrcPocessInstanceByIds(Long[] ids);

    /**
     * 删除流程实例信息
     * 
     * @param id 流程实例ID
     * @return 结果
     */
    public int deleteActGrcPocessInstanceById(Long id);
}
