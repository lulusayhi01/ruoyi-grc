package com.ruoyi.grc.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 损失事件+相关部门对象 lossevent_dept_relation
 * 
 * @author liuqi
 * @date 2021-01-21
 */
public class LosseventDeptRelation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 损失事件id */
    @Excel(name = "损失事件id")
    private Long losseventId;

    /** 相关部门ID */
    @Excel(name = "相关部门ID")
    private Long deptId;

    public void setLosseventId(Long losseventId) 
    {
        this.losseventId = losseventId;
    }

    public Long getLosseventId() 
    {
        return losseventId;
    }
    public void setDeptId(Long deptId) 
    {
        this.deptId = deptId;
    }

    public Long getDeptId() 
    {
        return deptId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("losseventId", getLosseventId())
            .append("deptId", getDeptId())
            .toString();
    }
}
