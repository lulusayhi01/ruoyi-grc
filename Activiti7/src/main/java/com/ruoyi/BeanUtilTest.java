package com.ruoyi;

import cn.hutool.core.bean.BeanPath;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.springframework.context.expression.MapAccessor;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.common.TemplateParserContext;
import org.springframework.expression.spel.standard.SpelExpression;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class BeanUtilTest {
    public static void main(String[] args) {
        Holiday holiday = new Holiday();
        holiday.setNum(4f);
        holiday.setHolidayName("如花");
        holiday.setId(1);
        holiday.setType("年假");
        holiday.setReason("想男友了");
        Map<String, Object> tempMap = new HashMap<String, Object>();
        tempMap.put("holiday", holiday);
        tempMap.put("flag", 1);

        BeanPath resolver = new BeanPath("holiday.num");
        Object result = resolver.get(tempMap);//ID为1
        System.out.println(result);


        String skipExpress="${value1==value2}";
        Map map = new HashMap<>();
        map.put("value1",1);
        map.put("value2",1);
        Boolean b = expressionParsing(skipExpress, map);
        System.out.println(b);

    }
    public static Boolean expressionParsing(String skipExpress, Map map){
        if (StringUtils.isBlank(skipExpress) && map.isEmpty()){
            return false;
        }
        ExpressionParser parser = new SpelExpressionParser();
        StandardEvaluationContext context = new StandardEvaluationContext();

        TemplateParserContext templateParserContext = new TemplateParserContext("${", "}");
        MapAccessor propertyAccessor = new MapAccessor();
        context.setVariables(map);
        context.setPropertyAccessors(Arrays.asList(propertyAccessor));

        SpelExpression expression = (SpelExpression) parser.parseExpression(skipExpress, templateParserContext);
        expression.setEvaluationContext(context);
        boolean value = expression.getValue(map,boolean.class);
        return value;
    }

    @Test
    public void testProcessDeploy2() {
        Holiday2 holiday = new Holiday2();
        holiday.setHolidayName("aaa");

    }

}
