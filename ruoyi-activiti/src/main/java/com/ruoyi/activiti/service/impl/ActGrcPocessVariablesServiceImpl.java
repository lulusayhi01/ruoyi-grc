package com.ruoyi.activiti.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.activiti.mapper.ActGrcPocessVariablesMapper;
import com.ruoyi.activiti.domain.ActGrcPocessVariables;
import com.ruoyi.activiti.service.IActGrcPocessVariablesService;

/**
 * 流程参数Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-12-06
 */
@Service
public class ActGrcPocessVariablesServiceImpl implements IActGrcPocessVariablesService 
{
    @Autowired
    private ActGrcPocessVariablesMapper actGrcPocessVariablesMapper;

    /**
     * 查询流程参数
     * 
     * @param id 流程参数ID
     * @return 流程参数
     */
    @Override
    public ActGrcPocessVariables selectActGrcPocessVariablesById(Long id)
    {
        return actGrcPocessVariablesMapper.selectActGrcPocessVariablesById(id);
    }

    /**
     * 查询流程参数列表
     * 
     * @param actGrcPocessVariables 流程参数
     * @return 流程参数
     */
    @Override
    public List<ActGrcPocessVariables> selectActGrcPocessVariablesList(ActGrcPocessVariables actGrcPocessVariables)
    {
        return actGrcPocessVariablesMapper.selectActGrcPocessVariablesList(actGrcPocessVariables);
    }

    /**
     * 新增流程参数
     * 
     * @param actGrcPocessVariables 流程参数
     * @return 结果
     */
    @Override
    public int insertActGrcPocessVariables(ActGrcPocessVariables actGrcPocessVariables)
    {
        return actGrcPocessVariablesMapper.insertActGrcPocessVariables(actGrcPocessVariables);
    }

    /**
     * 修改流程参数
     * 
     * @param actGrcPocessVariables 流程参数
     * @return 结果
     */
    @Override
    public int updateActGrcPocessVariables(ActGrcPocessVariables actGrcPocessVariables)
    {
        return actGrcPocessVariablesMapper.updateActGrcPocessVariables(actGrcPocessVariables);
    }

    /**
     * 批量删除流程参数
     * 
     * @param ids 需要删除的流程参数ID
     * @return 结果
     */
    @Override
    public int deleteActGrcPocessVariablesByIds(Long[] ids)
    {
        return actGrcPocessVariablesMapper.deleteActGrcPocessVariablesByIds(ids);
    }

    /**
     * 删除流程参数信息
     * 
     * @param id 流程参数ID
     * @return 结果
     */
    @Override
    public int deleteActGrcPocessVariablesById(Long id)
    {
        return actGrcPocessVariablesMapper.deleteActGrcPocessVariablesById(id);
    }
}
