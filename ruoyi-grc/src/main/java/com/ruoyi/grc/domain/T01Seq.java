package com.ruoyi.grc.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 编号生成对象 t01_seq
 * 
 * @author liuqi
 * @date 2021-01-15
 */
public class T01Seq extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 编号 */
    @Excel(name = "编号")
    private String code;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 前缀 */
    @Excel(name = "前缀")
    private String prefix;

    /** 序号 */
    @Excel(name = "序号")
    private Long seqIndex;
    //上一个序号
    private Long preSeqIndex;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date lastTime;
    //上一个天
    private Date preLastTime;


    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setPrefix(String prefix) 
    {
        this.prefix = prefix;
    }

    public String getPrefix() 
    {
        return prefix;
    }
    public void setSeqIndex(Long seqIndex) 
    {
        this.seqIndex = seqIndex;
    }

    public Long getSeqIndex() 
    {
        return seqIndex;
    }
    public void setLastTime(Date lastTime) 
    {
        this.lastTime = lastTime;
    }

    public Date getLastTime() 
    {
        return lastTime;
    }

    public Long getPreSeqIndex() {
        return preSeqIndex;
    }

    public void setPreSeqIndex(Long preSeqIndex) {
        this.preSeqIndex = preSeqIndex;
    }

    public Date getPreLastTime() {
        return preLastTime;
    }

    public void setPreLastTime(Date preLastTime) {
        this.preLastTime = preLastTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("code", getCode())
            .append("name", getName())
            .append("prefix", getPrefix())
            .append("seqIndex", getSeqIndex())
            .append("lastTime", getLastTime())
            .append("remark", getRemark())
            .toString();
    }
}
