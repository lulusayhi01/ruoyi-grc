package com.ruoyi.activiti.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 *  流程历史对象 act_grc_instance_historic
 * 
 * @author ruoyi
 * @date 2020-12-07
 */
public class ActGrcInstanceHistoric extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 流程id */
    @Excel(name = "流程id")
    private String proInstanceId;

    /** 节点id */
    @Excel(name = "节点id")
    private String elementId;

    /** 节点名称 */
    @Excel(name = "节点名称")
    private String elementName;

    /** 受理人 */
    @Excel(name = "受理人")
    private String assignee;

    /** $column.columnComment */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "受理人", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;


    public ActGrcInstanceHistoric() {

    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setProInstanceId(String proInstanceId) 
    {
        this.proInstanceId = proInstanceId;
    }

    public String getProInstanceId() 
    {
        return proInstanceId;
    }
    public void setElementId(String elementId) 
    {
        this.elementId = elementId;
    }

    public String getElementId() 
    {
        return elementId;
    }
    public void setElementName(String elementName) 
    {
        this.elementName = elementName;
    }

    public String getElementName() 
    {
        return elementName;
    }
    public void setAssignee(String assignee) 
    {
        this.assignee = assignee;
    }

    public String getAssignee() 
    {
        return assignee;
    }
    public void setStartTime(Date startTime) 
    {
        this.startTime = startTime;
    }

    public Date getStartTime() 
    {
        return startTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("proInstanceId", getProInstanceId())
            .append("elementId", getElementId())
            .append("elementName", getElementName())
            .append("assignee", getAssignee())
            .append("startTime", getStartTime())
            .append("remark", getRemark())
            .toString();
    }
}
