package com.ruoyi.activiti.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;

/**
 * 流程启动参数对象 act_grc_pocess_variables
 * 
 * @author ruoyi
 * @date 2020-12-06
 */
public class ActGrcPocessVariablesStart
{
    @NotBlank(message = "流程定义的key不能为空")
    private String befinitionKey;
    @NotNull(message = "业务id不能为空")
    private String businessId;
    @NotNull(message = "下一个节点的受理人不能为空")
    private String user;

    private String processDefinitionId;

    /** 参数 */
    private Map<String,Object> vars;

    public void setVars(Map<String, Object> vars) {
        this.vars = vars;
    }

    public Map<String, Object> getVars() {

        return vars;
    }

    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    public void setProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
    }

    public String getBefinitionKey() {
        return befinitionKey;
    }

    public String getBusinessId() {
        return businessId;
    }

    public String getUser() {
        return user;
    }

    public void setBefinitionKey(String befinitionKey) {
        this.befinitionKey = befinitionKey;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
