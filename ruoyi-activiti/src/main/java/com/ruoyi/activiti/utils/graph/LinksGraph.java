package com.ruoyi.activiti.utils.graph;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class LinksGraph {

    private int index;

    private int source;
    private int target;
    private LinkedList<String> value = new LinkedList<String>();
    private Map<String,Object> lineStyle ;

    public LinksGraph(int source, int target) {
        this.source = source;
        this.target = target;
    }

    public LinksGraph() {
    }


    public void appendLineStyle(String key,Object obj){
        if(this.lineStyle == null){
            this.lineStyle= new HashMap<String,Object>();
        }
        this.lineStyle.put(key,obj);


    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public void setValue(LinkedList<String> value) {
        this.value = value;
    }

    public void setSource(int source) {
        this.source = source;
    }

    public void setTarget(int target) {
        this.target = target;
    }

    public void setLineStyle(Map<String, Object> lineStyle) {
        this.lineStyle = lineStyle;
    }

    public LinkedList<String> getValue() {
        return value;
    }

    public int getSource() {
        return source;
    }

    public int getTarget() {
        return target;
    }

    public Map<String, Object> getLineStyle() {
        return lineStyle;
    }
}
