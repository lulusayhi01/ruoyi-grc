package com.ruoyi.grc.mapper;

import java.util.List;
import com.ruoyi.grc.domain.T21LdcItem;

/**
 * 损失明细Mapper接口
 * 
 * @author liuqi
 * @date 2021-01-14
 */
public interface T21LdcItemMapper 
{
    /**
     * 查询损失明细
     * 
     * @param id 损失明细ID
     * @return 损失明细
     */
    public T21LdcItem selectT21LdcItemById(Long id);

    /**
     * 查询损失明细列表
     * 
     * @param t21LdcItem 损失明细
     * @return 损失明细集合
     */
    public List<T21LdcItem> selectT21LdcItemList(T21LdcItem t21LdcItem);

    /**
     * 新增损失明细
     * 
     * @param t21LdcItem 损失明细
     * @return 结果
     */
    public int insertT21LdcItem(T21LdcItem t21LdcItem);

    /**
     * 修改损失明细
     * 
     * @param t21LdcItem 损失明细
     * @return 结果
     */
    public int updateT21LdcItem(T21LdcItem t21LdcItem);

    /**
     * 删除损失明细
     * 
     * @param id 损失明细ID
     * @return 结果
     */
    public int deleteT21LdcItemById(Long id);

    /**
     * 批量删除损失明细
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteT21LdcItemByIds(Long[] ids);
}
