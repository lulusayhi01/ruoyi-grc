package com.ruoyi;
import java.io.Serializable;
import java.util.Date;

/**
 * 请假实体类
 *
 * @author <a href="mailto:1900919313@qq.com">weiwei.xu</a>
 * @version 1.0
 * 2020-08-06 14:30
 */
public class Holiday implements Serializable {

    /**
     * 主键
     */
    private Integer id;

    /**
     * 申请人名字
     */
    private String holidayName;

    /**
     * 开始时间
     */
    private Date beginDate;

    /**
     * 结束时间
     */
    private Date endDate;

    /**
     * 请假天数
     */
    private Float num;

    /**
     * 请假事由
     */
    private String reason;

    /**
     * 请假类型：病假、婚假、丧假等
     */
    private String type;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHolidayName() {
        return holidayName;
    }

    public void setHolidayName(String holidayName) {
        this.holidayName = holidayName;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Float getNum() {
        return num;
    }

    public void setNum(Float num) {
        this.num = num;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Holiday{" +
                "id=" + id +
                ", holidayName='" + holidayName + '\'' +
                ", beginDate=" + beginDate +
                ", endDate=" + endDate +
                ", num=" + num +
                ", reason='" + reason + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
