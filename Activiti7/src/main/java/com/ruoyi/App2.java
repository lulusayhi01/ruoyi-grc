package com.ruoyi;

import com.googlecode.aviator.AviatorEvaluator;
import com.googlecode.aviator.Expression;
import org.activiti.engine.*;
import org.junit.Test;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Hello world!
 *
 */
public class App2
{
    public static String definitionKey="Process_1";
    public static void main(String[] args) {
        byte[] allocation1, allocation2;
        allocation1 = new byte[30900*1024];
        allocation2 = new byte[900*1024];
       /* Holiday holiday = new Holiday();
        holiday.setNum(4f);
        holiday.setHolidayName("如花");
        holiday.setId(1);
        holiday.setType("年假");
        holiday.setReason("想男友了");
        holiday.setBeginDate(new Date());

        //{${holiday.num <3}}
        String expression = "{${true}}"
                .replaceAll("\\{" ,"")
                .replaceAll("\\}" ,"")
                .replaceAll("\\$" ,"")
                ;
        // 编译表达式
        Expression compiledExp = AviatorEvaluator.compile(expression);

        Map<String, Object> env = new HashMap<>();
        env.put("holiday", holiday);

        Map<String, Object> env2 = new HashMap<>();
        env.put("holiday", holiday);

        Boolean result = (Boolean) compiledExp.execute(env);
        System.out.println(result);

        Boolean result2 = (Boolean) compiledExp.execute(env);
        System.out.println(result);*/

        /*StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("1111");
        stringBuffer.append("\r\n");
        stringBuffer.append("2222");

        System.out.println(stringBuffer);*/
        /*testString2();
        System.out.println("11111111111");*/
    }
    public static void testString2() {
        System.out.println("22222222222");
        System.out.println("33333");
        testString3();
        System.out.println("44444444");
        System.out.println("5555555");
    }
    public static void testString3() {
        System.out.println("22222222222");
        System.out.println("33333");
        System.out.println("44444444");
        System.out.println("5555555");
    }
    @Test
    public void testString() {
        System.out.println("xxxx");
        System.out.println("aaaa \r\n bbb");
    }


    @Test
    //任务查询
    public void testcompiledExp() {
        Holiday holiday = new Holiday();
        holiday.setNum(4f);
        holiday.setHolidayName("如花");
        holiday.setId(1);
        holiday.setType("年假");
        holiday.setReason("想男友了");
        holiday.setBeginDate(new Date());

        //{${holiday.num <3}}
        String expression = "{${holiday.num <3}}"
                .replaceAll("\\{" ,"")
                .replaceAll("\\}" ,"")
                .replaceAll("\\$" ,"")
                ;
        // 编译表达式
        Expression compiledExp = AviatorEvaluator.compile(expression);

        Map<String, Object> env = new HashMap<>();
        env.put("holiday", holiday);

        Map<String, Object> env2 = new HashMap<>();
        env.put("holiday", holiday);

        Boolean result = (Boolean) compiledExp.execute(env);
        System.out.println(result);

        Boolean result2 = (Boolean) compiledExp.execute(env);
        System.out.println(result);
    }

}
