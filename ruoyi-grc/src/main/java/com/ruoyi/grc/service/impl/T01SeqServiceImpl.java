package com.ruoyi.grc.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.grc.mapper.T01SeqMapper;
import com.ruoyi.grc.domain.T01Seq;
import com.ruoyi.grc.service.IT01SeqService;

/**
 * 编号生成Service业务层处理
 * 
 * @author liuqi
 * @date 2021-01-15
 */
@Service
public class T01SeqServiceImpl implements IT01SeqService 
{
    @Autowired
    private T01SeqMapper t01SeqMapper;

    /**
     * 查询编号生成
     * 
     * @param id 编号生成ID
     * @return 编号生成
     */
    @Override
    public T01Seq selectT01SeqById(Long id)
    {
        return t01SeqMapper.selectT01SeqById(id);
    }

    /**
     * 查询编号生成列表
     * 
     * @param t01Seq 编号生成
     * @return 编号生成
     */
    @Override
    public List<T01Seq> selectT01SeqList(T01Seq t01Seq)
    {
        return t01SeqMapper.selectT01SeqList(t01Seq);
    }

    /**
     * 新增编号生成
     * 
     * @param t01Seq 编号生成
     * @return 结果
     */
    @Override
    public int insertT01Seq(T01Seq t01Seq)
    {
        return t01SeqMapper.insertT01Seq(t01Seq);
    }

    /**
     * 修改编号生成
     * 
     * @param t01Seq 编号生成
     * @return 结果
     */
    @Override
    public int updateT01Seq(T01Seq t01Seq)
    {
        return t01SeqMapper.updateT01Seq(t01Seq);
    }

    /**
     * 批量删除编号生成
     * 
     * @param ids 需要删除的编号生成ID
     * @return 结果
     */
    @Override
    public int deleteT01SeqByIds(Long[] ids)
    {
        return t01SeqMapper.deleteT01SeqByIds(ids);
    }

    /**
     * 删除编号生成信息
     * 
     * @param id 编号生成ID
     * @return 结果
     */
    @Override
    public int deleteT01SeqById(Long id)
    {
        return t01SeqMapper.deleteT01SeqById(id);
    }
}
