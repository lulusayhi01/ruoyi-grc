package com.ruoyi.activiti.utils.graph;

import com.ruoyi.activiti.domain.ActGrcPocessVariables;

import java.util.LinkedList;
import java.util.List;

public class SeriesGraph {
    private LinkedList<DataGraph>  data = new LinkedList<DataGraph>();
    private LinkedList<LinksGraph> links = new LinkedList<LinksGraph>();

    private List<ActGrcPocessVariables> actGrcPocessVariables;



    public int getDataIdex(String name){
        int index=-1;


        DataGraph dataGraph = getDataGraph(name);
        if(dataGraph!=null){
             index = dataGraph.getIndex();
        }

        return  index;
    }

    public DataGraph getDataGraph(String name){


        for (DataGraph datum : data) {
            if(name.equals(datum.getElementid())){

                return datum;
            }
        }



        return  null;
    }

    public LinksGraph getLinksGraph(int source ,int target){
        for (LinksGraph link : links) {
            if(link.getSource()==source && link.getTarget()==target){
                return link;
            }
            if(link.getSource()==target && link.getTarget()==source){
                return link;
            }
        }


        return  null;
    }

    public List<ActGrcPocessVariables> getActGrcPocessVariables() {
        return actGrcPocessVariables;
    }

    public void setActGrcPocessVariables(List<ActGrcPocessVariables> actGrcPocessVariables) {
        this.actGrcPocessVariables = actGrcPocessVariables;
    }

    public LinkedList<DataGraph> getData() {
        return data;
    }

    public SeriesGraph() {
    }

    public void setData(LinkedList<DataGraph> data) {
        this.data = data;
    }

    public LinkedList<LinksGraph> getLinks() {
        return links;
    }

    public void setLinks(LinkedList<LinksGraph> links) {
        this.links = links;
    }
}
